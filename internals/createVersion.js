#!/usr/bin/env node
const fs = require('fs');
const { execSync } = require('child_process');
const pkg = require('../package.json');

const branch = execSync('git branch --show-current');
const data = `Project ${pkg.name}\nVersion ${pkg.version}\nBuild @${new Date().toGMTString().replace(/\s/g, '-')}\nBranch ${branch}`;

fs.writeFileSync(`${process.cwd()}/build/version.txt`, data, 'utf-8');
