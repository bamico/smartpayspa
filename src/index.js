import React from 'react';
import ReactDOM from 'react-dom';
import { createBrowserHistory } from 'history';
import MainContainer from './containers/MainContainer';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import './styles/scss/main.scss';
import store from './store/createStore';
// import proxy from './utils/proxy';

// ========================================================
// Render Setup
// ========================================================
const MOUNT_NODE = document.getElementById('root');
const authRoutes = require('./routes/index').default();
const noauthRoutes = require('./routes/index').createUnauthRoutes();

const history = createBrowserHistory();

const render = () => {
  ReactDOM.render(
    <MainContainer store={store} authRoutes={authRoutes} noauthRoutes={noauthRoutes} history={history} />,
    MOUNT_NODE,
  );
};
if (process.env === 'development') {
  if (module.hot) {
    // Development render functions
    // Setup hot module replacement
    module.hot.accept('./routes/index', () => setImmediate(() => {
      ReactDOM.unmountComponentAtNode(MOUNT_NODE);
      render();
    }));
  }
}
render();
// proxy();
