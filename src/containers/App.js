import React, { Component } from 'react';
import { BrowserRouter, Routes } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ScrollToTop from '../components/ScrollToTop';
import ErrorCodes from '../components/ErrorCodes';

class App extends Component {
  static propTypes = {
    authRoutes: PropTypes.objectOf(PropTypes.arrayOf(PropTypes.shape({
      component: PropTypes.func.isRequired,
    }))).isRequired,
    noauthRoutes: PropTypes.objectOf(PropTypes.arrayOf(PropTypes.shape({
      component: PropTypes.func.isRequired,
    }))).isRequired,
    history: PropTypes.object.isRequired,
    // store: PropTypes.object.isRequired,
    logged: PropTypes.bool.isRequired,
    backEndErrors: PropTypes.array,
    email: PropTypes.string,
  };

  static defaultProps = {
    backEndErrors: [],
    email: '',
  }

  state = {
    loading: true,
  };

  componentDidMount() {
    let prevLocation;
    this.props.history.listen((nextLocation) => {
      if (prevLocation !== nextLocation) {
        nextLocation.prevLocation = '/';
        if (prevLocation) {
          nextLocation.prevLocation = prevLocation.pathname;
        }
      }
      prevLocation = nextLocation;
    });
    this.state.loading = false;
  }

  shouldComponentUpdate() {
    return true;
  }

  componentWillUnmount() {
  }

  render() {
    const { authRoutes, noauthRoutes, logged } = this.props;

    if (!logged) { console.log(); }

    const loggedApp = (
      authRoutes.routes.map(component => (
        component
      ))
    );

    return (
      <div className="container-fluid mx-0">
        <div className="row px-0">
          <div className="col px-0">
            <BrowserRouter>
              {
                this.props.backEndErrors.length > 0
                && (
                  <ErrorCodes
                    errors={this.props.backEndErrors}
                    email={this.props.email}
                  />
                )
              }
              {/* <ErrorCodes
                errors={[
                  {
                    fieldName: 'person.name',
                    reason: 'CAN NOT BE EMPTY',
                    code: 10001,
                  },
                  {
                    fieldName: 'credentials.email',
                    reason: 'ALREADY FOUND IN DB',
                    code: 10005,
                  },
                  {
                    fieldName: 'enterprise.vat',
                    reason: 'ALREADY FOUND IN DB',
                    code: 10007,
                  },
                ]}
                email="sam@gmail.com"
              /> */}
              <ScrollToTop>
                <Routes>
                  {noauthRoutes.routes.map(({ component }) => (
                    component()
                  ))}
                  {loggedApp}
                </Routes>
              </ScrollToTop>
            </BrowserRouter>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  backEndErrors: state.errorForm.backEndErrors,
  email: state.errorForm.email,
});

export default connect(mapStateToProps)(App);
