import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import App from './App';
import { FormDataContextProvider } from '../store/form-context';
import itAppStrings from '../translations/lang-it.json';
import LanguageProvider from '../utils/LanguageProvider';

let checkToken = JSON.parse(window.sessionStorage.getItem('postObject'));
if (checkToken !== null && process.env.REACT_APP_ISMOCK === 'false') {
  checkToken = checkToken.token;
}
if (process.env.REACT_APP_ISMOCK === 'true' && checkToken === null) {
  checkToken = '0ffb8f14-4f94-4c5b-86ed-9f897904749a';
  window.sessionStorage.setItem('postObject', JSON.stringify({
    token: `${checkToken}`,
  }));
}
const translationMessages = {
  it: {
    ...itAppStrings,
  },
};

class MainContainer extends Component {
  static propTypes = {
    authRoutes: PropTypes.objectOf(PropTypes.arrayOf(PropTypes.shape({
      component: PropTypes.func.isRequired,
    }))).isRequired,
    noauthRoutes: PropTypes.objectOf(PropTypes.arrayOf(PropTypes.shape({
      component: PropTypes.func.isRequired,
    }))).isRequired,
    store: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
  };

  state = {
    language: {
      messages: translationMessages,
      locale: 'it',
    },
    logged: process.env.REACT_APP_ISMOCK === 'true',
  };

  componentDidMount() {
    if (checkToken !== null || process.env.REACT_APP_ISMOCK === 'true') {
      this.state.logged = true;
    }
  }

  render() {
    const { store } = this.props;

    return (
      <>
        <Provider store={store}>
          <LanguageProvider language={this.state.language}>
            <FormDataContextProvider>
              <App logged={this.state.logged} {...this.props} store={store} />
            </FormDataContextProvider>
          </LanguageProvider>
        </Provider>
      </>
    );
  }
}

export default MainContainer;
