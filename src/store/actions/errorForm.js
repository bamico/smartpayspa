export const actionTypes = {
  FORM_BACKEND_ERROR: 'FORM_BACKEND_ERROR',
  CLEAR_ERROR_FORM: 'CLEAR_ERROR_FORM',
  FORM_ERROR: 'FORM_ERROR',
  FORM_ERROR_INVALIDATE: 'FORM_ERROR_INVALIDATE',
  FORM_FIELD_ERROR: 'FORM_FIELD_ERROR',
  FORM_SET_CURRENT_MODEL: 'FORM_SET_CURRENT_MODEL',
};

export const backEndError = (errors, email) => ({
  type: actionTypes.FORM_BACKEND_ERROR,
  payload: { errors, email },

});

export const clearErrorForm = (message, fieldErrors, type) => ({
  type: actionTypes.CLEAR_ERROR_FORM,
  payload: {
    message,
    fieldErrors,
    type,
  },
});

export const formErrorInvalidate = payload => ({
  type: actionTypes.FORM_ERROR_INVALIDATE,
  payload: {
    ...payload,
  },
});

export const formError = payload => ({
  type: actionTypes.FORM_ERROR,
  payload: {
    ...payload,
  },
});

export const formFieldError = (formname, fieldname, message = false) => ({
  type: actionTypes.FORM_FIELD_ERROR,
  payload: {
    formname,
    fieldname,
    message,
  },
});

export const setCurrentModel = payload => ({
  type: actionTypes.FORM_SET_CURRENT_MODEL,
  payload,
});
