import { combineForms } from 'react-redux-form';

export const forgotPasswordForm = combineForms({
  ForgotPasswordEmail: {
    email: '',
  },
  ForgotPasswordAnswer: {
    answer: '',
  },
}, 'forgotPasswordForm');

export default forgotPasswordForm;
