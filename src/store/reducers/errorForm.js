import dotProp from 'dot-prop-immutable';
import { actionTypes } from '../actions/errorForm';

const ACTION_HANDLERS = {
  [actionTypes.FORM_ERROR_INVALIDATE]: (state, action) => {
    const newState = dotProp.set(state, `type`, action.payload.type);
    return dotProp.set(newState, `message`, action.payload.message);
  },

  [actionTypes.FORM_BACKEND_ERROR]: (state, action) => {
    let newState = dotProp.set(state, `backEndErrors`, action.payload.errors);
    newState = dotProp.set(newState, 'email', action.payload.email);
    return dotProp.set(newState, `error`, action.payload.error);
  },

  [actionTypes.CLEAR_ERROR_FORM]: (state) => {
    let newState = dotProp.set(state, `type`, '');
    newState = dotProp.set(newState, `messages`, '');
    return dotProp.set(newState, `fieldErrors`, '');
  },

  [actionTypes.FORM_SET_CURRENT_MODEL]: (state, action) => {
    let newState = state;
    if (action.payload === 'scaSmsOtpForm.OtpForm' && state.currentModel !== 'scaSmsOtpForm.OtpForm') {
      newState = dotProp.set(newState, `previousModel`, state.currentModel);
    }
    newState = dotProp.set(newState, `currentModel`, action.payload);
    return newState;
  },

  HANDLE_LOCATION_CHANGE: (state) => {
    let newState = dotProp.set(state, `type`, '');
    newState = dotProp.set(newState, `messages`, '');
    return dotProp.set(newState, `fieldErrors`, '');
  },
  FLOW_POPUP: (state, action) => {
    let newState = state;
    if (action.payload.popup !== '') {
      newState = dotProp.set(state, `type`, '');
      if (newState.message) {
        newState = dotProp.set(newState, `message`, '');
      } else if (newState.messages) {
        newState = dotProp.set(newState, `messages`, '');
      }
      return dotProp.set(newState, `fieldErrors`, '');
    }
    return newState;
  },
};

const initialState = { currentModel: '', previousModel: '' };
export default function errorFormReducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
