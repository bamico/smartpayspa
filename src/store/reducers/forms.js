import { combineForms } from 'react-redux-form';

export const registrationForm = combineForms({
  RegistrationPersonalDataForm: {
    name: '',
    surname: '',
    birthDate: '',
    birthPlace: '',
    mobilePhone: '',
    ssn: '',
    email: '',
    confirmEmail: '',
    privacyPolicyType: '',
  },
  RegistrationDocumentDataForm: {
    type: '',
    id: '',
    expirationDate: '',
    docFront: '',
    docBack: '',
    userPic: '',
  },
  RegistrationShopDataForm: {
    name: '',
    vat: '',
    businessSector: '',
    numberOfEmployees: '',
    city: '',
    county: '',
    streetWithNumber: '',
    zipCode: '',
  },
  RegistrationSecurityDataForm: {
    password: '',
    confirmPassword: '',
    secQuestion: '',
    secQuestionAnswer: '',
  },
}, 'registrationForm');

export default registrationForm;
