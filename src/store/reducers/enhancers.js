/*
 * enhancersReducer
 * Description
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 * @version 1.0.0
 * @author Bartolo Amico
 * @since 18-Feb-2022
 */
import dotProp from 'dot-prop-immutable';
import { actionTypes } from '../actions/enhancers';

const ACTION_HANDLERS = {
  [actionTypes.STATUS_IDLE]: (state, action) => {
    const newState = dotProp.set(state, 'status', action.payload);
    return newState;
  },
  [actionTypes.STATUS_LOADING]: (state) => {
    const newState = dotProp.set(state, 'status', 'loading');
    return newState;
  },
  [actionTypes.STATUS_SUCCESS]: (state) => {
    const newState = dotProp.set(state, 'status', 'success');
    return newState;
  },
  [actionTypes.STATUS_FAILURE]: (state) => {
    const newState = dotProp.set(state, 'status', 'error');
    return newState;
  },

};

// The initial state of the enhancers
const initialState = {
  error: '',
  status: 'idle',
};


const enhancers = (state = initialState, action) => {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
};

export default enhancers;
