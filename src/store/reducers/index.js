import { combineReducers } from 'redux';
import enhancersReducer from './enhancers';
import errorForm from './errorForm';
import * as formReducers from './forms';
// import * as formReducers2 from './forgotPassword';

export const makeRootReducer = injectedReducers => combineReducers({
  enhancers: enhancersReducer,
  errorForm,
  ...formReducers,
  // ...formReducers2,
  ...injectedReducers,
});

export const injectReducer = (store, { key, asyncReducer }) => {
  if (Reflect.has(store.asyncReducers, key)) return;

  store.asyncReducers[key] = asyncReducer;
  store.replaceReducer(makeRootReducer(store.asyncReducers));
};


export default makeRootReducer;
