import { applyMiddleware, compose, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import createReducer from './reducers';
import loggerMiddleware from './middleware/logger';
import monitorReducerEnhancer from './enhancers/monitorReducers';
import globals from './middleware/globals';
import { rootSaga } from './sagas';

const composeEnhancers = composeWithDevTools({
  trace: true, traceLimit: 25, serialize: { options: { undefined: true, function(fn) { return fn.toString(); } } } });
const sagaMiddleware = createSagaMiddleware();

const rootStore = (initialState = {}) => {
  // ======================================================
  // Middleware Configuration
  // ======================================================

  const middlewares = [sagaMiddleware, globals];
  if (process.env.NODE_ENV !== 'production' && process.env.DEBUG_MODE) {
    middlewares.push(loggerMiddleware);
  }

  // ======================================================
  // Store Enhancers
  // ======================================================
  const enhancers = [applyMiddleware(...middlewares)];
  // if (process.env.DEBUG_MODE) {
  enhancers.push(monitorReducerEnhancer);
  // }
  // ======================================================
  // Store Instantiation and HMR Setup
  // ======================================================
  const store = createStore(
    createReducer(),
    initialState,
    process.env.NODE_ENV === 'development' ? composeEnhancers(...enhancers) : compose(...enhancers),
  );

  store.asyncReducers = {};

  if (module.hot) {
    module.hot.accept('./reducers', () => {
      const defaultReducers = require('./reducers').default; // eslint-disable-line global-require
      store.replaceReducer(defaultReducers(store.asyncReducers));
    });
  }
  store.runSaga = sagaMiddleware.run;
  store.injectedSagas = new Map();
  store.runSaga(rootSaga);

  return store;
};

export default rootStore();
