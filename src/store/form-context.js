import React, { createContext, useState } from 'react';
import PropTypes from 'prop-types';

const FormDataContext = createContext({
  step: 0,
  formTitle: 'Registration',
  plusStep: () => {},
  minusStep: () => {},
});

export function FormDataContextProvider(props) {
  const [form1Data, setForm1Data] = useState({});
  const [nameUser, setName] = useState('');
  const [form2Data, setForm2Data] = useState({});
  const [form3Data, setForm3Data] = useState({});
  const [form4Data, setForm4Data] = useState({});

  let finalData = {};
  const [formStep, setFormStep] = useState(1);
  const [title, setFormTitle] = useState('Registration');

  const [currentUrl, setCurrentUrl] = useState(window.location.href);

  if (currentUrl !== window.location.href) {
    setFormStep(1);
  }

  function form1DataHandler(userData1) {
    setForm1Data({
      name: userData1.firstName,
      surname: userData1.lastName,
      phone: userData1.phone,
      id: userData1.id,
      email: userData1.email,
    });
  }

  function form2DataHandler(userData2) {
    setForm2Data({
      docType: userData2.docType,
      docId: userData2.docId,
      docExpire: userData2.docExpire,
      docFront: userData2.docFront,
      docBack: userData2.docBack,
      userPic: userData2.userPic,
    });
  }

  function form3DataHandler(userData3) {
    setForm3Data({
      shopName: userData3.shopName,
      shopCity: userData3.shopCity,
      shopProvince: userData3.shopProvince,
      shopRoute: userData3.shopRoute,
      shopCap: userData3.shopCap,
      shopCode: userData3.shopCode,
    });
  }

  function form4DataHandler(userData4) {
    setForm4Data({
      password1: userData4.password1,
      password2: userData4.password2,
      question: userData4.question,
      answer: userData4.answer,
    });
  }

  function addUserDataHandler() {
    finalData = {
      name: form1Data.name,
      surname: form1Data.surname,
      phone: form1Data.phone,
      id: form1Data.id,
      email: form1Data.email,
      docType: form2Data.docType,
      docId: form2Data.docId,
      docFront: form2Data.docFront,
      docBack: form2Data.docBack,
      userPic: form2Data.userPic,
      docExpire: form2Data.docExpire,
      shopName: form3Data.shopName,
      shopCity: form3Data.shopCity,
      shopProvince: form3Data.shopProvince,
      shopRoute: form3Data.shopRoute,
      shopCap: form3Data.shopCap,
      shopCode: form3Data.shopCode,
      password1: form4Data.password1,
      password2: form4Data.password2,
      question: form4Data.question,
      answer: form4Data.answer,
    };
    setName(form1Data.name);
    return finalData;
  }

  function increaseStep() {
    setFormStep(formStep + 1);
  }

  function decreaseStep() {
    setFormStep(formStep - 1);
  }

  function urlHandler(url) {
    setCurrentUrl(url);
    console.log(currentUrl);
  }

  function stepResetHandler() {
    setFormStep(1);
  }

  const context = {
    step: formStep,
    url: currentUrl,
    formTitle: title,
    nameUser,
    setUrl: urlHandler,
    stepReset: stepResetHandler,
    addData: addUserDataHandler,
    plusStep: increaseStep,
    minusStep: decreaseStep,
    addForm1Data: form1DataHandler,
    addForm2Data: form2DataHandler,
    addForm3Data: form3DataHandler,
    addForm4Data: form4DataHandler,
    setTitle: setFormTitle,
  };

  return (
    <FormDataContext.Provider value={context}>
      {props.children}
    </FormDataContext.Provider>
  );
}

FormDataContextProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export default FormDataContext;
