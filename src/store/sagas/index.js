import { all } from 'redux-saga/effects';

export function* rootSaga() {
  yield all([

  ]);
}

export function injectSaga(store, { key, asyncSaga }) {
  const isInjected = k => store.injectedSagas.has(k);
  if (isInjected(key)) return;
  const task = store.runSaga(asyncSaga);
  store.injectedSagas.set(key, task);
}
