import { axiosInstance as axios } from '@api';
import { handlingResponse } from '@utils/globalMethods';
import { defaultHeaders } from '../index';
/* eslint-disable global-require */
if (process.env.REACT_APP_ISMOCK === 'true') { require('@mock/registration/registration'); }
/* eslint-enable no-unused-vars */

export const uploadData = async (params) => {
  let data;
  defaultHeaders['Content-Type'] = 'multipart/form-data';

  try {
    const options = { headers: { 'Content-Type': 'multipart/form-data' } };
    data = await axios.post('http://smartpay-02.nets.local:8080/api/users', params, options);
    // console.log(data);
    return handlingResponse(data);
    /* .then((res) => {
        console.log(res);
    })
    .catch((err) => {
        console.log(err);
    }); */
  /* data = await axios({
      method: 'post',
      url: `/users`,
      data: params,
      headers: { ...defaultHeaders },
      withCredentials: true,
    });
    return handlingResponse(data);
  }  */
  } catch (error) {
    console.debug(error);
    return handlingResponse(error);
  }
};

export default uploadData;
