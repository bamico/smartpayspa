import { axiosInstance as axios } from '@api';
import { handlingResponse } from '@utils/globalMethods';
/* eslint-disable global-require */
if (process.env.REACT_APP_ISMOCK === 'true') { require('@mock/profile/profile'); }

export const sendEmail = async (params) => {
  let data;

  try {
    const options = { headers: { 'Content-Type': 'text/plain' } };
    data = await axios.put('http://smartpay-02.nets.local:8080/api/secquestion', params, options);
    console.log(data);
    return handlingResponse(data);
  } catch (error) {
    console.log(error);
    return handlingResponse(error);
  }
};

export const sendData = async (params) => {
  let data;

  try {
    const options = { headers: { 'Content-Type': 'application/json' } };
    data = await axios.post('http://smartpay-02.nets.local:8080/api/recover', params, options);
    console.log(data);
    return handlingResponse(data);
  } catch (error) {
    console.log(error);
    return handlingResponse(error);
  }
};

export default sendData;
