import { axiosInstance as axios } from '@api';
import { handlingResponse } from '@utils/globalMethods';
import { defaultHeaders } from '../index';
/* eslint-disable global-require */
if (process.env.REACT_APP_ISMOCK) { require('@mock/profile/profile'); }

export const getProfileInfo = async (params) => {
  let data;
  try {
    data = await axios({
      method: 'get',
      url: `/profiles/${params}`,
      headers: { ...defaultHeaders },
    });
    return handlingResponse(data);
  } catch (error) {
    return handlingResponse(error);
  }
};

export default getProfileInfo;
