/**
 *
 * Header
 *
 * @version 1.0.0
 * @author
 * @since
 */

import React, { useState } from 'react';
// import { Link } from 'react-router-dom';
import ConfirmHandler from '../../../../components/ConfirmModal';

const Header = () => {
  const [modalState, setModalState] = useState(false);

  const setModalFalse = () => {
    setModalState(false);
  };

  const modalOnConfirm = () => {
    setModalState(false);
    window.location.href = process.env.REACT_APP_REDIRECT;
  };

  const redirectLogout = () => {
    window.location.href = '/';
    sessionStorage.removeItem('token');
  };

  const redirectLogin = () => {
    window.location.href = '/';
    sessionStorage.removeItem('token');
  };

  // login
  const rawToken = sessionStorage.getItem('token');

  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light">
        <div className="container-fluid">
          <a className="navbar-brand fw-bold" href="/">SmartPay</a>
          <div className="collapse navbar-collapse" id="navbarText">
            <span className="navbar-text">
              {/* {props.hideCloseButton ? '' : (
                <Button className="btn-link" onclick={setModalTrue}>Chiudi</Button>
              )
              } */}
            </span>
            {rawToken !== null && (
              <span className="navbar-button">
                <button type="button" className="btn rounded-pill bg-primary text-white" onClick={redirectLogout}>Logout</button>
              </span>
            )}
            {rawToken === null && (
              <span className="navbar-button">
                <button type="button" className="btn rounded-pill bg-primary text-white" onClick={redirectLogin}>Login</button>
              </span>
            )}
            {/* <span className="navbar-button">
              {!props.buttonStatus && <button type="button" className="btn  rounded-pill bg-primary text-white" onClick={props.login}>Login</button>}
            </span>
            <span className="navabar-btn">
              {props.buttonStatus && <button type="button" className="btn  rounded-pill bg-danger text-white" onClick={props.logout}>Logout</button>}
            </span> */}
            {/* {props.buttonStatus
            && (
              <span className="navbar-text">
                <Link className="btn-link mx-5" to="/profile/info">Profile</Link>
              </span>
            )} */}
          </div>
        </div>
      </nav>
      {modalState && (
        <ConfirmHandler
          onConfirm={modalOnConfirm}
          onDeny={setModalFalse}
          title="Stop Compiling?"
          message="Are you sure you want to stop compiling the form?"
          buttonDeny="Cancel"
          buttonConfirm="Stop Compiling"
        />
      )}
    </>
  );
};

Header.propTypes = {
  // login: PropTypes.func.isRequired,
  // logout: PropTypes.func.isRequired,
  // buttonStatus: PropTypes.bool,
  // actions: PropTypes.object,
};

Header.defaultProps = {
  hideCloseButton: false,
  // buttonStatus: false,
  // actions: {},
};
export default Header;
