import { connect } from 'react-redux';
import Header from './Header';
import { setToken, deleteToken } from '../../modules/ProfileActions';

const mapDispatchToProps = (dispatch, ownProps) => ({
  setToken: () => { dispatch(setToken()); },
  deleteToken: () => { dispatch(deleteToken()); },
  closeAction: (payload) => {
    if (/intro/.test(window.location.pathname)) {
      window.sessionStorage.clear();
      window.location = '/';
    } else {
      dispatch(ownProps.closeAction(payload));
    }
  },
  goBack: (payload) => {
    dispatch(ownProps.goBack(payload));
  },
});

const mapStateToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps, (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  own: ownProps,
  actions: dispatchProps,
}))(Header);
