import { connect } from 'react-redux';
import LoginProfileInfo from './LoginProfileInfo';
import { deleteToken, setToken } from '../../modules/ProfileActions';

const mapDispatchToProps = dispatch => ({
  setToken: () => { dispatch(setToken()); },
  deleteToken: () => { dispatch(deleteToken()); },
});

const mapStateToProps = () => ({
});

export default connect(mapStateToProps, mapDispatchToProps, (stateProps, dispatchProps, ownProps) => (
  {
    ...stateProps,
    own: ownProps,
    actions: dispatchProps,
  }
))(LoginProfileInfo);
