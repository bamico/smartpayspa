import React, { useState, useEffect } from 'react';
import { getProfileInfo } from '@api/profile/profile';
import Header from '../Header/Header';

const LoginProfileInfo = () => {
  //
  const [profile, setList] = useState({});
  const [document, setDoc] = useState([]);
  const [enterprise, setEnterprise] = useState([]);
  const [address, setAddress] = useState([]);
  const [request, setRequest] = useState({});

  // prova per deploy

  const rawToken = sessionStorage.getItem('token');
  const profileId = atob(rawToken);
  console.log(rawToken);
  console.log(profileId);

  if (rawToken !== null) {
    useEffect(() => {
      getProfileInfo(profileId).then((r) => {
        setList(r.response.data);
        setDoc({ ...r.response.data.documents[0] });
        setEnterprise({ ...r.response.data.enterprise });
        setAddress({ ...r.response.data.enterprise.address });
        setRequest({ ...r.response.data.request });
      });
    }, [rawToken]);
  }

  // const [btnIsShown, setBtnIsShown] = useState(false);
  // const showBtnHandler = () => {
  //   setBtnIsShown(true);
  // };


  return (
    <>
      <Header />
      {rawToken !== null
      && (
        <div>
          <div className="container">

            {/* Welcome and name */}
            <div className="row my-3">
              <h2>
                Welcome,
                {' '}
                {profile.name}
              </h2>
            </div>

            {/* Request Table */}
            <div className="mt-5 mx-0 p-0 card row">
              <div className="card-body">
                <table>
                  <thead>
                    <tr className="col-12">
                      <th className="pe-5">Request Type</th>
                      <th className="px-5">Request Date</th>
                      <th className="px-5 text-end">Request Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td className="pe-5 col-2">{request.type}</td>
                      <td className="px-5 col-2">{request.date}</td>
                      <td className="pe-5 col-7 text-end">{request.status}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

            {/* Personal Details */}
            <div className="row mt-5">
              <h5 className="d-flex">
                <span className="d-inline col-6 text-start">Personal Details</span>
                <div className="container">
                  <div className="row justify-content-md-end">
                    {/* <div className="col col-lg-2">
                      {btnIsShown && <button className="btn btn-outline-primary text-uppercase" type="button">edit</button>}
                    </div> */}
                  </div>
                </div>
              </h5>
              <div className="row card">
                <div className="card-body">
                  <div className="text-start mb-3">
                    <span className="d-block mb-1 text-secondary">Name</span>
                    <span>{profile.name}</span>
                  </div>
                  <div className="text-start mb-3">
                    <span className="d-block mb-1 text-secondary">Last Name</span>
                    <span>{profile.surname}</span>
                  </div>
                  <div className="text-start mb-3">
                    <span className="d-block mb-1 text-secondary">Birthday</span>
                    <span>{profile.birthDate}</span>
                  </div>
                  <div className="text-start mb-3">
                    <span className="d-block mb-1 text-secondary">Birthplace</span>
                    <span>{profile.birthPlace}</span>
                  </div>
                  <div className="text-start mb-3">
                    <span className="d-block mb-1 text-secondary">Phone</span>
                    <span>{profile.mobilePhone}</span>
                  </div>
                  <div className="text-start mb-3">
                    <span className="d-block mb-1 text-secondary">Fiscal Code</span>
                    <span>{profile.ssn}</span>
                  </div>
                  <div className="text-start mb-3">
                    <span className="d-block mb-1 text-secondary">Email</span>
                    <span>{profile.email}</span>
                  </div>
                </div>
              </div>
            </div>

            {/* Document Details */}
            <div className="row mt-5">
              <h5 className="d-flex">
                <span className="d-inline col-6 text-start">Document</span>
                <div className="container">
                  <div className="row justify-content-md-end">
                    {/* <div className="col col-lg-2">
                      {btnIsShown && <button className="btn btn-outline-primary text-uppercase" type="button">edit</button>}
                    </div> */}
                  </div>
                </div>
              </h5>
              <div className="row card">
                <div className="card-body">
                  <div className="text-start mb-3">
                    <span className="d-block mb-1 text-secondary">{document.type || ''}</span>
                    <span>{document.id}</span>
                  </div>
                  <div className="text-start mb-3">
                    <span className="d-block mb-1 text-secondary">Expires on</span>
                    <span>{document.expirationDate}</span>
                  </div>
                </div>
              </div>
            </div>

            {/* Shop Details */}
            <div className="row mt-5">
              <h5 className="d-flex">
                <span className="d-inline col-6 text-start">Business Details</span>
                <div className="container">
                  <div className="row justify-content-md-end">
                    {/* <div className="col col-lg-2">
                      {btnIsShown && <button className="btn btn-outline-primary text-uppercase" type="button">edit</button>}
                    </div> */}
                  </div>
                </div>
              </h5>
              <div className="row card">
                <div className="card-body">
                  <div className="text-start mb-3">
                    <span className="d-block mb-1 text-secondary">Business Name</span>
                    <span>{enterprise.name}</span>
                  </div>
                  <div className="text-start mb-3">
                    <span className="d-block mb-1 text-secondary">Business Address</span>
                    <span className="d-block">{address.streetWithNumber}</span>
                    <span className="d-block">
                      {address.city}
                      {'('}
                      {address.county}
                      {')'}
                      {' '}
                      {address.zipCode}
                    </span>

                  </div>
                  <div className="text-start mb-3">
                    <span className="d-block mb-1 text-secondary">VAT</span>
                    <span>{enterprise.vat}</span>
                  </div>
                </div>
              </div>
            </div>

            {/* Password */}
            <div className="row my-5">
              <h5 className="d-flex">
                <span className="d-inline col-6 text-start">Password</span>
                <div className="container">
                  <div className="row justify-content-md-end">
                    {/* <div className="col col-lg-2">
                      {btnIsShown && <button className="btn btn-outline-primary text-uppercase" type="button">edit</button>}
                    </div> */}
                  </div>
                </div>
              </h5>
              <div className="row card">
                <div className="card-body">
                  <div className="text-start mb-3">
                    <span className="d-block mb-1 text-secondary">Password</span>
                    <span>************</span>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      )}
      {rawToken === null && <h1 className="text-center">Error</h1>}
    </>
  );
};


LoginProfileInfo.propTypes = {
  // Add here some propTypes
};

LoginProfileInfo.defaultProps = {
  // Add here some default propTypes values
};

export default LoginProfileInfo;
