
const ACTION_HANDLERS = {
  SET_TOKEN: (state) => {
    localStorage.setItem('token', 'cfgDTYGchfyguhvGFYGUHJGUhJguyihJGgkhvhjguygjfgugHGUGGU');
    return state;
  },
  DELETE_TOKEN: (state) => {
    localStorage.setItem('token', '');
    return state;
  },
};

const initialState = {
  token: '',
};

export default function login(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
