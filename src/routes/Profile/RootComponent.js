import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Footer from '../../components/Footer/Footer';
import LoginProfileInfo from './components/LoginProfileInfo/LoginProfileInfoContainer';

const LoginRoot = () => (
  <Routes>
    <Route
      path="/info"
      element={(
        <>
          <LoginProfileInfo />
          <Footer />
        </>
      )}
    />
  </Routes>
);

export default LoginRoot;
