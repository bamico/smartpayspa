import React from 'react';
import { FormattedMessage } from 'react-intl';

const formattedMessage = {
  title:
  <FormattedMessage
    id="smartpay.registration.title"
    defaultMessage="Registration"
  />,
  subtitle:
  <FormattedMessage
    id="smartpay.registration.personalData.subtitle"
    defaultMessage="Personal Data"
  />,
  /* notValidMessage:
  <FormattedMessage
    id="smartpay.registration.error.email"
    defaultMessage="email address not valid"
  />, */
  emailLabel:
  <FormattedMessage
    id="smartpay.registration.email.emailLabel"
    defaultMessage="Insert your email"
  />,
  required:
  <FormattedMessage
    id="smartpay.registration.error.required"
    defaultMessage="Field required"
  />,
  onlyLetter:
  <FormattedMessage
    id="smartpay.registration.error.onlyLetter"
    defaultMessage="Insert only letter, min 2 max 30"
  />,
  cityInvalid:
  <FormattedMessage
    id="smartpay.registration.error.city"
    defaultMessage="Please check the field (case sensitive)"
  />,
  cityRequired:
  <FormattedMessage
    id="smartpay.registration.error.cityRequired"
    defaultMessage="Field Required! "
  />,
  phoneInvalid:
  <FormattedMessage
    id="smartpay.registration.error.phone"
    defaultMessage="Invalid phone number"
  />,
  fiscalcodeInvalid:
  <FormattedMessage
    id="smartpay.registration.error.fiscalcode"
    defaultMessage="Invalid Fiscal Code"
  />,
  emailInvalid:
  <FormattedMessage
    id="smartpay.registration.error.email"
    defaultMessage="email address not valid"
  />,
  errorConfirmEmail:
  <FormattedMessage
    id="smartpay.registration.error.confirmemail"
    defaultMessage="The email address must bee the same"
  />,
  privacy:
  <FormattedMessage
    id="smartpay.registration.error.privacy"
    defaultMessage="You must accept the privacy policy"
  />,
};

export default formattedMessage;
