/**
 *
 * Logout
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import { Form, Control, Errors } from 'react-redux-form';
import { isRequired, isName, isSurname, isMobilePhone, isFiscalCode, isEmail, chooseCity, confirmEmail, privacyCheck } from '@utils/validatorForm';
import options from '@utils/models/cities.json';
import InputTextField from '@components/InputTextField';
import SelectTextField from '@components/SelectTextField';
import StepProgress from '@components/StepProgress';
import messages from './languageModule';

const RegistrationPersonalData = (props) => {
  const CalendarInput = (props) => {
  // Date validation 18yo
    let today = new Date();
    today.setDate(today.getDate() - 1);
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0');
    const yyyy = today.getFullYear();
    today = `${yyyy - 18 }-${ mm }-${ dd }`;

    return <input className="my-input" type="date" {...props} max={today} />;
  };

  const navigate = useNavigate();

  const handleSubmit = () => {
    props.actions.nextStep();
    navigate('/registration/document-data');
  };

  const handleCities = props => options.some(item => item.nome === props);

  return (
    <>
      <StepProgress myprop={1} />
      <div className="container-xl px-4 pb-5">
        <div className="row justify-content-center">
          <div className="col-lg-7">
            <div className="card shadow-lg border-0 rounded-lg mt-5">
              <div className="card-header justify-content-center">
                <h3>{messages.title}</h3>
              </div>
              <div className="card-body">
                <p className="card-text">Please, fill the form with your personal information</p>
                <Form
                  model={props.modelForm}
                  onFocus={props.actions.setCurrentModel}
                  onSubmit={() => { handleSubmit(); }}
                  autoComplete="off"
                  className="row g-3 mt-3"
                  validators={{
                    '': { confirmEmail },
                    birthPlace: { chooseCity },
                    email: { isEmail },
                    confirmEmail: { isEmail },
                    ssn: { isFiscalCode },
                    mobilePhone: { isMobilePhone },
                    privacyPolicyCheck: { privacyCheck },
                  }}
                  validateOn="change"
                >

                  <div className="col-md-6">
                    <InputTextField
                      label="First Name"
                      model=".name"
                      name="name"
                      validators={{
                        isRequired,
                        isName,
                      }}
                      mapProps={{
                        className: ({ fieldValue }) => (`${fieldValue.focus ? 'focus' : ''} ${!fieldValue.valid && !fieldValue.focus && !fieldValue.pristine ? 'is-invalid ' : ''}form-control`),
                      }}
                      messages={{
                        isRequired: messages.required,
                        isName: messages.onlyLetter,
                      }}
                    />
                  </div>
                  <div className="col-md-6">
                    <InputTextField
                      model=".surname"
                      name="surname"
                      label="Last Name"
                      validators={{
                        isRequired,
                        isName,
                        isSurname,
                      }}
                      mapProps={{
                        className: ({ fieldValue }) => (`${fieldValue.focus ? 'focus' : ''} ${!fieldValue.valid && !fieldValue.focus && !fieldValue.pristine ? 'is-invalid ' : ''}form-control`),
                      }}
                      messages={{
                        isRequired: messages.required,
                        isName: messages.onlyLetter,
                      }}
                    />

                  </div>
                  <div className="col-md-6">
                    <label className="form-label" htmlFor="birthDate">Birthday</label>
                    <Control
                      model=".birthDate"
                      component={CalendarInput}
                      name="birthDate"
                      validators={{
                        isRequired,
                      }}
                      mapProps={{
                        className: ({ fieldValue }) => (`${fieldValue.focus ? 'focus' : ''} ${!fieldValue.valid && !fieldValue.focus && !fieldValue.pristine ? 'is-invalid ' : ''}form-control`),
                      }}
                    />
                    <Errors
                      show={{ touched: true, focus: false, pristine: false }}
                      model=".birthDate"
                      className="invalid-feedback"
                      messages={{
                        isRequired: messages.required,
                      }}
                    />
                    <div className="form-text mb-1">Insert your date of birth</div>
                  </div>
                  <div className="col-md-6">
                    <SelectTextField
                      model=".birthPlace"
                      name="birthPlace"
                      id="city"
                      value={props.city}
                      label="Birth Place"
                      validators={{
                        chooseCity,
                        handleCities,
                      }}
                      mapProps={{
                        className: ({ fieldValue }) => (`${fieldValue.focus ? 'focus' : ''} ${!fieldValue.valid && !fieldValue.focus && !fieldValue.pristine ? 'is-invalid ' : ''}form-control`),
                      }}
                      messages={{
                        handleCities: messages.cityInvalid,
                        chooseCity: messages.cityRequired,
                      }}
                      data={options}
                    />
                    <div className="form-text">Insert your place of birth</div>
                  </div>
                  <div className="col-md-6">
                    <InputTextField
                      model=".mobilePhone"
                      name="mobilePhone"
                      label="Phone"
                      validators={{
                        isRequired,
                        isMobilePhone,
                      }}
                      mapProps={{
                        className: ({ fieldValue }) => (`${fieldValue.focus ? 'focus' : ''} ${!fieldValue.valid && !fieldValue.focus && !fieldValue.pristine ? 'is-invalid ' : ''}form-control`),
                      }}
                      messages={{
                        isRequired: messages.required,
                        isMobilePhone: messages.phoneInvalid,
                      }}
                    />
                    <div className="form-text">Insert your personal cellphone number</div>
                  </div>
                  <div className="col-md-6">
                    <InputTextField
                      model=".ssn"
                      name="ssn"
                      label="Fiscal Code"
                      validators={{
                        isRequired,
                        isFiscalCode,
                      }}
                      mapProps={{
                        className: ({ fieldValue }) => (`${fieldValue.focus ? 'focus' : ''} ${!fieldValue.valid && !fieldValue.focus && !fieldValue.pristine ? 'is-invalid ' : ''}form-control`),
                      }}
                      length={16}
                      messages={{
                        isRequired: messages.required,
                        isFiscalCode: messages.fiscalcodeInvalid,
                      }}
                    />
                    <div className="form-text">Insert your personal fiscal code</div>
                  </div>
                  <div className="col-md-6">
                    <InputTextField
                      model=".email"
                      name="email"
                      label="Email"
                      validators={{
                        isEmail,
                        isRequired,
                      }}
                      onPaste={e => e.preventDefault()}
                      onCopy={e => e.preventDefault()}
                      onCut={e => e.preventDefault()}
                      onDrag={e => e.preventDefault()}
                      onDrop={e => e.preventDefault()}
                      autocomplete="off"
                      mapProps={{
                        className: ({ fieldValue }) => (`${fieldValue.focus ? 'focus' : ''} ${!fieldValue.valid && !fieldValue.focus && !fieldValue.pristine ? 'is-invalid ' : ''}form-control`),
                      }}
                      messages={{
                        isRequired: messages.required,
                        isEmail: messages.emailInvalid,
                      }}
                    />
                    <div className="form-text">
                      Insert your email
                      <br />
                      (you can either use your shop&#39;s or personal email)
                    </div>

                  </div>
                  <div className="col-md-6">
                    <InputTextField
                      name="confirmEmail"
                      label="Re-enter Email"
                      model=".confirmEmail"
                      validators={{
                        isEmail,
                        isRequired,
                        confirmEmail,
                      }}
                      mapProps={{
                        className: ({ fieldValue }) => (`${fieldValue.focus ? 'focus' : ''} ${!fieldValue.valid && !fieldValue.focus && !fieldValue.pristine ? 'is-invalid ' : ''}form-control`),
                      }}
                      onPaste={e => e.preventDefault()}
                      onCopy={e => e.preventDefault()}
                      onCut={e => e.preventDefault()}
                      onDrag={e => e.preventDefault()}
                      onDrop={e => e.preventDefault()}
                      autocomplete="off"
                    />
                    <Errors
                      show={field => field.touched && !field.pristine && !field.focus}
                      className="error"
                      model={props.modelForm}
                      messages={{
                        isEmail: messages.emailInvalid,
                        confirmEmail: messages.errorConfirmEmail,
                      }}
                    />
                    <div className="form-text">Confirm your email</div>
                  </div>

                  <div>
                    <label className="form-check-label" htmlFor="privacyCheck">
                      I agree to the
                      <a target="_blank" href="https://www.nexi.it/privacy.html" rel="noreferrer"> Privacy Policy </a>
                    </label>
                    <Control.checkbox
                      model=".privacyPolicyCheck"
                      name="privacyPolicyCheck"
                      validators={{
                        privacyCheck,
                      }}
                      mapProps={{
                        className: ({ fieldValue }) => (`${fieldValue.focus ? 'focus' : ''} ${!fieldValue.valid && !fieldValue.focus && !fieldValue.pristine ? 'is-invalid ' : ''}form-check-input`),
                      }}
                      value={false}
                      validateOn="change"
                    />
                    <Errors
                      show={{ touched: true, focus: false, pristine: false }}
                      model=".privacyPolicyCheck"
                      className="invalid-feedback"
                      messages={{
                        privacyCheck: messages.privacy,
                      }}
                    />
                  </div>
                  <div className="col-12">
                    <button
                      type="submit"
                      className="btn btn-primary btn-block"
                      disabled={!props.isValid}
                    >
                      Next
                    </button>
                  </div>
                </Form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};


RegistrationPersonalData.propTypes = {
  // Add here some propTypes
  actions: PropTypes.object.isRequired,
  modelForm: PropTypes.string.isRequired,
  city: PropTypes.string.isRequired,
  isValid: PropTypes.bool,

};

RegistrationPersonalData.defaultProps = {
  // Add here some default propTypes values
  isValid: false,
};

export default RegistrationPersonalData;
