import { connect } from 'react-redux';
import { setCurrentModel } from '@store/actions/errorForm';
import { registrationNextStep } from '../../modules/RegistrationActions';
import RegistrationPersonalData from './RegistrationPersonalData';

const FORM_MODEL = 'registrationForm.RegistrationPersonalDataForm';

const mapDispatchToProps = dispatch => ({
  nextStep: () => { dispatch(registrationNextStep('UPLOAD_DOCS')); },
  setCurrentModel: () => {
    dispatch(setCurrentModel(FORM_MODEL));
  },
});

const mapStateToProps = state => ({
  isValid: state.registrationForm.forms.RegistrationPersonalDataForm.$form.valid,
  city: state.registrationForm.RegistrationPersonalDataForm.birthPlace,
  modelForm: FORM_MODEL,
});

export default connect(mapStateToProps, mapDispatchToProps, (stateProps, dispatchProps, ownProps) => (
  {
    ...stateProps,
    own: ownProps,
    actions: dispatchProps,
  }
))(RegistrationPersonalData);
