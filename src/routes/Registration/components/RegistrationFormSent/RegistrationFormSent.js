/**
 *
 * Logout
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

function RegistrationFormSent(props) {
  const space = ' ';
  return (
    <div className="container py-5 my-5">
      <div className="row d-flex justify-content-center card-custom my-5">
        <div className="tick">
          <p className="pt-3 px-4 text-light">&#10004;</p>
        </div>
        <div className="col-12 py-4">
          <h2 className="text-capitalize text-success">
            Thank you
            {space}
            {props.name}
          </h2>
          <p>
            Your form has been sent correctly
            <br />
            In a few days you will get a response
          </p>
        </div>
        <div className="col-12 py-5">
          <Link to="/">
            <button type="button" className="btn btn-primary btn-lg px-5 ">
              Go to profile
            </button>
          </Link>
        </div>
      </div>
    </div>
  );
}

RegistrationFormSent.propTypes = {
  // Add here some propTypes
  name: PropTypes.string.isRequired,
};

export default RegistrationFormSent;
