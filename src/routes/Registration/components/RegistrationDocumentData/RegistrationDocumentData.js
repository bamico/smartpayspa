/**
 *
 * Logout
 *
 */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import { Form, Control, Errors } from 'react-redux-form';
import { isRequired, documentFormat } from '@utils/validatorForm';
// import { imageRecognition } from '@utils/globalMethods';
import InputTextField from '@components/InputTextField/InputTextField';
import StepProgress from '@components/StepProgress';
import DropFileInput from '@components/DropFileInput';
import messages from './languageModule';


const RegistrationDocumentData = (props) => {
  // Expire date validation (after tomorrow)
  const CalendarInput = (props) => {
    let today = new Date();
    today.setDate(today.getDate());
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0');
    const yyyy = today.getFullYear();
    today = `${yyyy }-${ mm }-${ dd}`;
    return <input className="my-input" type="date" {...props} min={today} />;
  };

  const navigate = useNavigate();

  const handleBack = () => {
    props.actions.prevStep();
    navigate('/registration/personal-data');
  };

  const [docFront, setDocFront] = useState();
  const [docBack, setDocBack] = useState();
  const [userPic, setUserPic] = useState();

  const handleSubmit = () => {
    props.actions.nextStep();
    props.actions.setDocFront(docFront);
    props.actions.setDocBack(docBack);
    props.actions.setUserPic(userPic);
    navigate('/registration/shop-data');
  };

  // Drag 'n' drop
  const onFileChange = (files) => {
    const file = files[0];
    setDocFront(false);
    return new Promise((resolve) => {
      if (file) {
        let baseURL = '';
        // Make new FileReader
        const reader = new FileReader();
        // Convert the file to base64 text
        reader.readAsDataURL(file);
        // on reader load somthing...
        reader.onload = () => {
          // Make a fileInfo Object
          baseURL = reader.result;
          setDocFront(baseURL);
          resolve(baseURL);
        };
      }
    });
  };

  const onFileChange1 = (files) => {
    const file = files[0];
    setDocBack(false);
    return new Promise((resolve) => {
      if (file) {
        let baseURL = '';
        // Make new FileReader
        const reader = new FileReader();
        // Convert the file to base64 text
        reader.readAsDataURL(file);
        // on reader load somthing...
        reader.onload = () => {
          // Make a fileInfo Object
          baseURL = reader.result;
          setDocBack(baseURL);
          resolve(baseURL);
        };
      }
    });
  };

  const onFileChange2 = (files) => {
    const file = files[0];
    setUserPic(false);
    return new Promise((resolve) => {
      if (file) {
        let baseURL = '';
        // Make new FileReader
        const reader = new FileReader();
        // Convert the file to base64 text
        reader.readAsDataURL(file);
        // on reader load somthing...
        reader.onload = () => {
          // Make a fileInfo Object
          baseURL = reader.result;
          setUserPic(baseURL);
          resolve(baseURL);
        };
      }
    });
  };

  return (
    <>
      <StepProgress myprop={2} />
      <div className="container-xl px-4">
        <div className="row justify-content-center">
          <div className="col-lg-7">
            <div className="card shadow-lg border-0 rounded-lg mt-5">
              <div className="card-header justify-content-center">
                <h3>{messages.title}</h3>
              </div>
              <div className="card-body">
                <p className="card-text">Please, fill the form with your document information</p>
                <Form
                  model={props.modelForm}
                  onFocus={props.actions.setCurrentModel}
                  onSubmit={() => { handleSubmit(); }}
                  autoComplete="off"
                  validators={{
                    '': { documentFormat },
                  }}
                  className="row g-3 mt-3"
                  validateOn="change"
                >
                  {/* Document Type */}
                  <div className="col-md-6">
                    <label className="form-label" htmlFor="type">Select a Document</label>
                    <Control.select
                      model=".type"
                      name="type"
                      validators={{
                        isRequired,
                      }}
                      mapProps={{
                        className: ({ fieldValue }) => (`${fieldValue.focus ? 'focus' : ''} ${!fieldValue.valid && !fieldValue.focus && !fieldValue.pristine ? 'is-invalid ' : ''}form-control`),
                      }}
                      label="sdsfdsafjdsafijdsaoi"
                      validateOn="change"
                    >
                      <option disabled />
                      <option value="physical identity card">Identity Card</option>
                      <option value="electronic identity card">Electronic Identity Card</option>
                      <option value="passport">Passport</option>
                      {/* <option value="DriversLicense">{'Driver\'s License'}</option> */}
                    </Control.select>
                    <Errors
                      show={{ touched: true, focus: false, pristine: false }}
                      model={`${props.modelForm}.type`}
                      className="invalid-feedback"
                      messages={{
                        isRequired: messages.required,
                      }}
                    />
                    <small className="form-text d-block text-muted">Choose the type of document you want to insert</small>
                  </div>

                  {/* Document ID */}
                  <div className="col-md-6">
                    <InputTextField
                      model=".id"
                      name="docId"
                      label="Document ID"
                      validators={{
                        isRequired,
                        documentFormat,
                      }}
                      mapProps={{
                        className: ({ fieldValue }) => (`${fieldValue.focus ? 'focus' : ''} ${!fieldValue.valid && !fieldValue.focus && !fieldValue.pristine ? 'is-invalid ' : ''}form-control`),
                      }}
                    />
                    <Errors
                      show={field => field.touched && !field.pristine && !field.focus}
                      className="error"
                      model={props.modelForm}
                      messages={{
                        documentFormat: 'Formato non corretto',
                      }}
                    />

                    <small className="form-text d-block text-muted">Insert the ID of the document you choose above</small>
                  </div>

                  {/* Expiration date */}
                  <div className="col-md-6">
                    <label className="fs-6 pb-2" htmlFor="expirationDate">Expiration Date</label>
                    <Control
                      model=".expirationDate"
                      component={CalendarInput}
                      name="expirationDate"
                      validators={{
                        isRequired,
                      }}
                      mapProps={{
                        className: ({ fieldValue }) => (`${fieldValue.focus ? 'focus' : ''} ${!fieldValue.valid && !fieldValue.focus && !fieldValue.pristine ? 'is-invalid ' : ''}form-control`),
                      }}
                      validateOn="change"
                    />
                    <Errors
                      show={{ touched: true, focus: false, pristine: false }}
                      model={`${props.modelForm}.expirationDate`}
                      className="invalid-feedback"
                      messages={{
                        isRequired: messages.required,
                      }}
                    />
                    <small className="form-text d-block text-muted">Insert the expiration date of the document you choose above</small>
                  </div>

                  {/* Drag 'n' drop */}
                  <div className="col-mt-5 py-5">
                    <div className="row d-flex align-items-center">
                      <div className="col-12 py-3">
                        <p className="h2">UPLOAD DOCUMENT</p>
                        <p>Please upload the front and back views of your document</p>
                        <p>Document should be in .png or . jpeg format and not exceed 2MB</p>
                      </div>
                      <p className="h4">Front</p>
                      <div className="col-12 d-flex justify-content-center py-2">
                        <DropFileInput
                          validateOn="change"
                          model=".docFront"
                          name="docFront"
                          value={{ docFront }}
                          onFileChange={files => onFileChange(files)}
                          id="front"
                        />
                      </div>
                      <p className="h4">Back</p>
                      <div className="col-12 d-flex justify-content-center py-2">
                        <DropFileInput
                          validateOn="change"
                          model=".docBack"
                          name="docBack"
                          value={{ docBack }}
                          onFileChange={files => onFileChange1(files)}
                          id="back"
                        />

                      </div>
                      <p className="h4">Photo</p>
                      <p>Upload a your pic to help us to check your identity</p>
                      <div className="col-12 d-flex justify-content-center py-2">
                        <DropFileInput
                          validateOn="change"
                          model=".userPic"
                          name="userPic"
                          value={{ userPic }}
                          onFileChange={files => onFileChange2(files)}
                          id="self"
                        />
                      </div>
                    </div>
                  </div>

                  <div className="fields pt-4 mt-1 mb-5 d-flex justify-content-around">
                    <button
                      className="btn btn-primary"
                      type="button"
                      onClick={handleBack}
                    >
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-left" viewBox="0 0 16 16">
                        <path fillRule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z" />
                      </svg>
                      Back
                    </button>
                    {!!docBack && !!docFront && !!userPic === true ? (
                      <button
                        className="btn btn-primary"
                        type="submit"
                        disabled={!props.isValid}
                      >
                        Next
                      </button>
                    ) : (
                      <button
                        className="btn btn-primary"
                        type="submit"
                        disabled
                      >
                        Next
                      </button>
                    )}
                  </div>
                </Form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};


RegistrationDocumentData.propTypes = {
  // Add here some propTypes
  actions: PropTypes.object.isRequired,
  modelForm: PropTypes.string.isRequired,
  isValid: PropTypes.bool,
};

RegistrationDocumentData.defaultProps = {
  // Add here some default propTypes values
  isValid: false,
};

export default RegistrationDocumentData;
