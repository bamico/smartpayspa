import { connect } from 'react-redux';
import { actions } from 'react-redux-form';
import { setCurrentModel } from '@store/actions/errorForm';
import { registrationNextStep, registrationPreviousStep } from '../../modules/RegistrationActions';
import RegistrationDocumentData from './RegistrationDocumentData';

const FORM_MODEL = 'registrationForm.RegistrationDocumentDataForm';

const mapDispatchToProps = dispatch => ({
  nextStep: () => { dispatch(registrationNextStep('SHOP_DATA')); },
  prevStep: () => { dispatch(registrationPreviousStep('SHOP_DATA')); },
  setCurrentModel: () => {
    dispatch(setCurrentModel(FORM_MODEL));
  },
  setDocFront: (image) => {
    dispatch(actions.change('registrationForm.RegistrationDocumentDataForm.docFront', image));
  },
  setDocBack: (image) => {
    dispatch(actions.change('registrationForm.RegistrationDocumentDataForm.docBack', image));
  },
  setUserPic: (image) => {
    dispatch(actions.change('registrationForm.RegistrationDocumentDataForm.userPic', image));
  },
});

const mapStateToProps = state => ({
  isValid: state.registrationForm.forms.RegistrationDocumentDataForm.$form.valid,
  docType: state.registrationForm.forms.RegistrationDocumentDataForm.docType,
  modelForm: FORM_MODEL,
});

export default connect(mapStateToProps, mapDispatchToProps, (stateProps, dispatchProps, ownProps) => (
  {
    ...stateProps,
    own: ownProps,
    actions: dispatchProps,
  }
))(RegistrationDocumentData);
