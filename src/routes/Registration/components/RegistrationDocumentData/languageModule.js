import React from 'react';
import { FormattedMessage } from 'react-intl';

const formattedMessage = {
  title:
  <FormattedMessage
    id="smartpay.registration.title"
    defaultMessage="Registration"
  />,
  subtitle:
  <FormattedMessage
    id="smartpay.registration.documentData.subtitle"
    defaultMessage="Document Data"
  />,
  required:
  <FormattedMessage
    id="smartpay.registration.error.required"
    defaultMessage="Field required"
  />,
  id:
  <FormattedMessage
    id="smartpay.registration.error.required"
    defaultMessage="Invalid format, insert id number"
  />,
  cid:
  <FormattedMessage
    id="smartpay.registration.error.required"
    defaultMessage="Invalid format, insert cid number"
  />,
  license:
  <FormattedMessage
    id="smartpay.registration.error.required"
    defaultMessage="Invalid format, insert drivers license number"
  />,
  passport:
  <FormattedMessage
    id="smartpay.registration.error.required"
    defaultMessage="Invalid format, insert passport number"
  />,
};

export default formattedMessage;
