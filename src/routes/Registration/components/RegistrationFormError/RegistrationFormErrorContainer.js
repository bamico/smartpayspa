import { connect } from 'react-redux';
import { setCurrentModel } from '@store/actions/errorForm';
import { registrationNextStep } from '../../modules/RegistrationActions';
import RegistrationFormError from './RegistrationFormError';

const FORM_MODEL = 'registrationForm.RegistrationFormErrorForm';

const mapDispatchToProps = dispatch => ({
  nextStep: () => { dispatch(registrationNextStep('UPLOAD_DOCS')); },
  setCurrentModel: () => {
    dispatch(setCurrentModel(FORM_MODEL));
  },
});

const mapStateToProps = state => ({
  name: state.registrationForm.RegistrationPersonalDataForm.name,
  myprop: state.registrationForm,
});

export default connect(mapStateToProps, mapDispatchToProps, (stateProps, dispatchProps, ownProps) => (
  {
    ...stateProps,
    own: ownProps,
    actions: dispatchProps,
  }
))(RegistrationFormError);
