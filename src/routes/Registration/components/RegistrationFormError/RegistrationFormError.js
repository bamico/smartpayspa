/**
 *
 * Logout
 *
 */
import React from 'react';
import { Link } from 'react-router-dom';

function RegistrationFormError(props) {
  console.log('Printing Props', props);
  return (
    <div className="container py-5 my-5">
      <div className="row d-flex justify-content-center card-custom my-5">
        <div className="tick bg-danger">
          <p className="pt-3 px-4 text-light">X</p>
        </div>
        <div className="col-12 py-4">
          <h2 className="text-capitalize text-danger">
            An error has occurred
          </h2>
          <p>Please try again later</p>
        </div>
        <div className="col-12 py-5">
          <Link to="/">
            <button type="button" className="btn btn-primary btn-lg px-5 ">
              Go home
            </button>
          </Link>
        </div>
      </div>
    </div>
  );
}

export default RegistrationFormError;
