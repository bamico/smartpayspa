import { connect } from 'react-redux';
import { backEndError, setCurrentModel } from '@store/actions/errorForm';
import { registrationNextStep, registrationPreviousStep } from '../../modules/RegistrationActions';
import RegistrationSecurityData from './RegistrationSecurityData';

const FORM_MODEL = 'registrationForm.RegistrationSecurityDataForm';

const mapDispatchToProps = dispatch => ({
  nextStep: () => { dispatch(registrationNextStep('FORM_SENT')); },
  prevStep: () => { dispatch(registrationPreviousStep('FORM_SENT')); },
  setCurrentModel: () => {
    dispatch(setCurrentModel(FORM_MODEL));
  },
  backEndError: (errors, email) => { dispatch(backEndError(errors, email)); },
});

const mapStateToProps = state => ({
  isValid: state.registrationForm.forms.RegistrationSecurityDataForm.$form.valid,
  formValues: state.registrationForm.RegistrationSecurityDataForm,
  modelForm: FORM_MODEL,
  data: state.registrationForm,
});

export default connect(mapStateToProps, mapDispatchToProps, (stateProps, dispatchProps, ownProps) => (
  {
    ...stateProps,
    own: ownProps,
    actions: dispatchProps,
  }
))(RegistrationSecurityData);
