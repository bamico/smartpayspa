import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import uploadData from '@api/registration/registration';
import category from '@utils/models/shopCategory.json';
import { Form, Control, Errors } from 'react-redux-form';
import { isRequired, isPassword, confirmPassword, isPasswordLongEnough, isPasswordLongTooMuch, isAnswer } from '@utils/validatorForm';
import StepProgress from '@components/StepProgress';
import RequirementsBar from '@components/RequirementsBar';
import InputTextField from '@components/InputTextField/InputTextField';
import Button from '@components/Button';
import messages from './languageModule';
import whereToRedirect from '../../../../utils/models/whereToRedirect.json';

const RegistrationSecurityData = (props) => {
  const navigate = useNavigate();
  const [error, setError] = useState('');

  const handleSubmit = () => {
    const formData = new FormData();

    const { RegistrationPersonalDataForm, RegistrationDocumentDataForm, RegistrationShopDataForm, RegistrationSecurityDataForm } = props.data;
    const business = RegistrationShopDataForm.businessSector;
    const cat = category.filter(c => c.nome === business)[0].codice;
    const registration = {
      person: { ...RegistrationPersonalDataForm,
        privacyPolicyType: 'standard',
      },
      document: {
        type: RegistrationDocumentDataForm.type,
        id: RegistrationDocumentDataForm.id,
        expirationDate: RegistrationDocumentDataForm.expirationDate,
      },
      enterprise: {
        name: RegistrationShopDataForm.name,
        address: {
          streetWithNumber: RegistrationShopDataForm.streetWithNumber,
          city: RegistrationShopDataForm.city,
          zipCode: RegistrationShopDataForm.zipCode,
          county: RegistrationShopDataForm.county,
        },
        vat: RegistrationShopDataForm.vat,
        businessSector: cat,
        numberOfEmployees: RegistrationShopDataForm.numberOfEmployees,
      },
      credentials: {
        ...RegistrationSecurityDataForm,
        email: RegistrationPersonalDataForm.email,
      },
    };

    function dataURLtoFile(dataUrl, filename) {
      const arr = dataUrl.split(',');
      const mime = arr[0].match(/:(.*?);/)[1];
      const type = mime.split('/')[1];
      const byteString = atob(arr[1]);
      let n = byteString.length;
      const u8arr = new Uint8Array(n);
      while (n >= 0) {
        u8arr[n] = byteString.charCodeAt(n);
        n -= 1;
      }

      return new File([u8arr], `${filename}.${type}`, { type: mime });
    }
    formData.append('registration', new Blob([JSON.stringify(registration)], {
      type: 'application/json',
    }));
    formData.append('documentFront', dataURLtoFile(props.data.forms.RegistrationDocumentDataForm.docFront.value, 'docFront'));
    formData.append('documentBack', dataURLtoFile(props.data.forms.RegistrationDocumentDataForm.docBack.value, 'docBack'));
    formData.append('picture', dataURLtoFile(props.data.forms.RegistrationDocumentDataForm.userPic.value, 'userPic'));

    uploadData(formData)
      .then((r) => {
        const response = r;
        if ((response.error !== undefined && response.error.status >= 500) || response.response === undefined) {
          setError('Something went wrong. Please try again later.');
        } else if (response.response.status === 200 || response.response.status === 201 || response.error.status === 200 || response.error.status === 201) {
          props.actions.nextStep();
          props.actions.backEndError();
          navigate('/registration/form-sent');
        } else if (response.error !== undefined && response.error.status === 400) {
          // Redirects user to earliest step where there was an error
          // and then shows list of errors as toasts to user on redirected page
          let step = 5;
          response.error.data.errors.forEach((error) => {
            console.debug(error.fieldName);
            console.debug(whereToRedirect[0]);
            console.log(Object.prototype.hasOwnProperty.call(whereToRedirect[0], error.fieldName));
            if (Object.prototype.hasOwnProperty.call(whereToRedirect[0], error.fieldName)) {
              const fieldStep = whereToRedirect[0][error.fieldName];
              if (step > fieldStep) step = fieldStep;
            } else {
              console.debug('Unknown error');
            }
          });
          props.actions.backEndError(response.error.data.errors, RegistrationPersonalDataForm.email);
          switch (step) {
          case 1: {
            navigate('/registration/personal-data');
            break;
          }
          case 2: {
            navigate('/registration/document-data');
            break;
          }
          case 3: {
            navigate('/registration/shop-data');
            break;
          }
          case 4: {
            navigate('/registration/security-data');
            break;
          }
          default: {
            navigate('/registration/form-sent-error');
          }
          }
        } else {
          navigate('/registration/form-sent-error');
        }
      })
      .catch((e) => {
        console.log('FAILURE!! ', e);
        navigate('/registration/form-sent-error');
      });
  };

  const handleBack = () => {
    props.actions.prevStep();
    navigate('/registration/shop-data');
  };
  return (
    <>
      <StepProgress myprop={4} />

      <div className="container-xl px-4">
        <div className="row justify-content-center">
          <div className="col-lg-7">
            <div className="card shadow-lg border-0 rounded-lg mt-5">
              <div className="card-header justify-content-center">
                <h3>{messages.title}</h3>
              </div>
              <div className="card-body">
                <h3>Choose a Password</h3>
                <p className="card-text">It will be used to log in to your personal account.</p>
                <div className="container">
                  <div className="row">
                    <p variant="subtitle2">Password must be between 8 and 30 characters and satisfy the requirments</p>
                  </div>
                  <RequirementsBar
                    requirementsList={[
                      {
                        id: 1,
                        label: <p variant="subtitle2">uppercase</p>,
                        isChecked: /[A-Z]+/.test(props.formValues ? props.formValues.password : ''),
                      },
                      {
                        id: 2,
                        label: <p variant="subtitle2">lowercase</p>,
                        isChecked: /[a-z]+/.test(props.formValues ? props.formValues.password : ''),
                      },
                      {
                        id: 3,
                        label: <p variant="subtitle2">number</p>,
                        isChecked: /[0-9]+/.test(props.formValues ? props.formValues.password : ''),
                      },
                      {
                        id: 4,
                        label: (
                          <div>
                            <p variant="subtitle2">special character</p>
                          </div>),
                        isChecked: /[~!@#$%^&*_\-+=\\`|(){}[\]:;"'<>,.?/ ]+/.test(props.formValues ? props.formValues.password : ''),
                      },

                    ]}
                  />
                </div>
                <Form
                  model={props.modelForm}
                  onFocus={props.actions.setCurrentModel}
                  onSubmit={() => { handleSubmit(); }}
                  autoComplete="off"
                  className="row g-3 mt-3"
                  validators={{
                    '': { confirmPassword },
                  }}
                  validateOn="change"
                >
                  <div className="col-md-6">
                    <InputTextField
                      type="password"
                      model=".password"
                      name="password"
                      label="Password"
                      validators={{
                        isPassword,
                        isRequired,
                        isPasswordLongEnough,
                        isPasswordLongTooMuch,
                      }}
                      mapProps={{
                        className: ({ fieldValue }) => (`${fieldValue.focus ? 'focus' : ''} ${!fieldValue.valid && !fieldValue.focus && !fieldValue.pristine ? 'is-invalid ' : ''}form-control`),
                      }}
                      messages={{
                        isPassword: 'Password is not valid',
                        isPasswordLongEnough: 'Must be at least 8 characters',
                        isPasswordLongTooMuch: 'Must be less than 36 characters',
                      }}
                      onCopy={e => e.preventDefault()}
                      onCut={e => e.preventDefault()}
                      onDrag={e => e.preventDefault()}
                      autocomplete="off"
                      ignore={['onpaste']}
                      validateOn="change"
                    />
                  </div>
                  <div className="col-md-6">
                    <InputTextField
                      type="password"
                      model=".confirmPassword"
                      name="confirmPassword"
                      label="Re-insert Password"
                      validators={{
                        isPassword,
                        isRequired,
                        isPasswordLongEnough,
                        isPasswordLongTooMuch,
                        confirmPassword,
                      }}
                      validateOn="blur"
                      onPaste={e => e.preventDefault()}
                      onCopy={e => e.preventDefault()}
                      onCut={e => e.preventDefault()}
                      onDrag={e => e.preventDefault()}
                      onDrop={e => e.preventDefault()}
                      autocomplete="off"
                      mapProps={{
                        className: ({ fieldValue }) => (`${fieldValue.focus ? 'focus' : ''} ${!fieldValue.valid && !fieldValue.focus && !fieldValue.pristine ? 'is-invalid ' : ''}form-control`),
                      }}
                      messages={{
                        isPassword: 'Password is not valid',
                        confirmPassword: 'Passwords do not match',
                        isPasswordLongEnough: 'Must be at least 8 characters',
                        isPasswordLongTooMuch: 'Must be less than 36 characters',
                      }}
                    />
                    <Errors
                      show={field => field.touched && !field.pristine && !field.focus}
                      className="error"
                      model={props.modelForm}
                      messages={{
                        isPassword: 'Password is not valid',
                        confirmPassword: 'Passwords do not match',
                      }}
                    />
                  </div>
                  <div>
                    <h3>Security Question</h3>
                    <p>Choose a security question. It will be used to recover your password is case of troubles.</p>
                  </div>

                  <div className="col-md-6">
                    <label className="form-check-label mb-2" htmlFor="question">Question:</label>
                    <Control.select
                      model=".secQuestion"
                      name="secQuestion"
                      validators={{
                        isRequired,
                      }}
                      mapProps={{
                        className: ({ fieldValue }) => (`${fieldValue.focus ? 'focus' : ''} ${!fieldValue.valid && !fieldValue.focus && !fieldValue.pristine ? 'is-invalid ' : ''}form-control`),
                      }}
                      validateOn="change"
                    >
                      <option value="">Choose a question</option>
                      <option>What is the name of your childhood pet?</option>
                      <option>What high school did you attend?</option>
                      <option>What is the name of your first school?</option>
                      <option>What was your favourite food as a child?</option>
                    </Control.select>
                    <Errors
                      show={{ touched: true, focus: false, pristine: false }}
                      model=".question"
                      className="invalid-feedback"
                      messages={{
                        isRequired: messages.required,
                      }}

                    />
                  </div>

                  <div className="col-md-6">
                    <InputTextField
                      model=".secQuestionAnswer"
                      name="secQuestionAnswer"
                      label="Your Answer:"
                      validators={{
                        isRequired,
                        isAnswer,
                      }}
                      mapProps={{
                        className: ({ fieldValue }) => (`${fieldValue.focus ? 'focus' : ''} ${!fieldValue.valid && !fieldValue.focus && !fieldValue.pristine ? 'is-invalid ' : ''}form-control`),
                      }}
                      messages={{
                        isRequired: messages.required,
                        isAnswer: messages.answerInvalid,
                      }}
                    />
                  </div>

                  <div className="fields pt-4 mt-1 mb-2 d-flex justify-content-around">
                    <Button
                      class="btn btn-primary"
                      type="button"
                      onclick={handleBack}
                    >
                      Back
                    </Button>
                    <Button
                      class="btn-primary"
                      type="submit"
                      isDisabled={!props.isValid}
                    >
                      Submit
                    </Button>
                  </div>
                  <span className="error" id={error}><h2>{error}</h2></span>
                </Form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};


RegistrationSecurityData.propTypes = {
  // Add here some propTypes
  actions: PropTypes.object.isRequired,
  modelForm: PropTypes.string.isRequired,
  isValid: PropTypes.bool,
  data: PropTypes.object.isRequired,
  formValues: PropTypes.object.isRequired,
};

RegistrationSecurityData.defaultProps = {
  // Add here some default propTypes values
  isValid: false,
};

export default RegistrationSecurityData;
