import React from 'react';
import { FormattedMessage } from 'react-intl';

const formattedMessage = {
  title:
  <FormattedMessage
    id="smartpay.registration.title"
    defaultMessage="Registration"
  />,
  subtitle:
  <FormattedMessage
    id="smartpay.registration.securityData.subtitle"
    defaultMessage="Security Data"
  />,
  required:
  <FormattedMessage
    id="smartpay.registration.error.required"
    defaultMessage="Field required"
  />,
  passwordInvalid:
  <FormattedMessage
    id="smartpay.registration.error.password"
    defaultMessage="The password must contain minimum 8 characters, at least one number and a special character"
  />,
  answerInvalid:
  <FormattedMessage
    id="smartpay.registration.error.answer"
    defaultMessage="Invalid length or characters"
  />,
};

export default formattedMessage;
