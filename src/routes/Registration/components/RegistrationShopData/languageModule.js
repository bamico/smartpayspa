import React from 'react';
import { FormattedMessage } from 'react-intl';

const formattedMessage = {
  title:
  <FormattedMessage
    id="smartpay.registration.title"
    defaultMessage="Registration"
  />,
  subtitle:
  <FormattedMessage
    id="smartpay.registration.shopData.subtitle"
    defaultMessage="Shop Data"
  />,
  required:
  <FormattedMessage
    id="smartpay.registration.error.required"
    defaultMessage="Field required"
  />,
  nameInvalid:
  <FormattedMessage
    id="smartpay.registration.error.name"
    defaultMessage="Invalid character"
  />,
  shopCodeInvalid:
  <FormattedMessage
    id="smartpay.registration.error.shopCode"
    defaultMessage="Invalid shop code"
  />,
  shopProvInvalid:
  <FormattedMessage
    id="smartpay.registration.error.shopProv"
    defaultMessage="Invalid province"
  />,
  shopRouteInvalid: // There is no regex for this field yet
  <FormattedMessage
    id="smartpay.registration.error.shopRoute"
    defaultMessage="Invalid route"
  />,
  shopPostalCodeInvalid:
  <FormattedMessage
    id="smartpay.registration.error.shopCap"
    defaultMessage="Invalid character"
  />,
  provinceInvalid:
  <FormattedMessage
    id="smartpay.registration.error.province"
    defaultMessage="Invalid province"
  />,
  cityInvalid:
  <FormattedMessage
    id="smartpay.registration.error.city"
    defaultMessage="Please check the field (case sensitive)"
  />,
  cityRequired:
  <FormattedMessage
    id="smartpay.registration.error.cityRequired"
    defaultMessage="Field Required! "
  />,
};

export default formattedMessage;
