/**
 *
 * Logout
 *
 */
import React from 'react';
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import { Form, Control, Errors } from 'react-redux-form';
import { isRequired, chooseCity, isVAT, isPostalCode, isProvince } from '@utils/validatorForm';
import options from '@utils/models/cities.json';
import counties from '@utils/models/counties.json';
import category from '@utils/models/shopCategory.json';
import StepProgress from '@components/StepProgress';
import InputTextField from '@components/InputTextField/InputTextField';
import SelectTextField from '@components/SelectTextField/SelectTextField';
import SelectTextFieldCounty from '@components/SelectTextField/SelectTextFieldCounty';
import messages from './languageModule';

const RegistrationShopData = (props) => {
  const navigate = useNavigate();

  const handleSubmit = () => {
    props.actions.nextStep();
    navigate('/registration/security-data');
  };
  /* const code = props.sector.split('/')[0];
  const name = props.sector.split('/')[1];
  console.log(code);
  console.log(name); */

  const handleBack = () => {
    props.actions.prevStep();
    navigate('/registration/document-data');
  };

  const handleCities = props => options.some(item => item.nome === props);

  return (
    <>
      <StepProgress myprop={3} />
      <div className="container-xl px-4">
        <div className="row justify-content-center">
          <div className="col-lg-7">
            <div className="card shadow-lg border-0 rounded-lg mt-5">
              <div className="card-header justify-content-center">
                <h3>{messages.title}</h3>
              </div>
              <div className="card-body">
                <div className="mt-5 mx-5">
                  <h3 className="mt-5">Business Details</h3>
                </div>
                <p className="card-text">Please provide the details of the business for which you are requesting a payment system</p>
                <Form
                  model={props.modelForm}
                  onFocus={props.actions.setCurrentModel}
                  onSubmit={() => { handleSubmit(); }}
                  autoComplete="off"
                  className="row g-3 mt-3"
                  validators={{}}
                  validateOn="change"
                >

                  <div className="col-md-6">
                    <InputTextField
                      model=".name"
                      name="name"
                      label="Shop Name"
                      validators={{
                        isRequired,
                      }}
                      mapProps={{
                        className: ({ fieldValue }) => (`${fieldValue.focus ? 'focus' : ''} ${!fieldValue.valid && !fieldValue.focus && !fieldValue.pristine ? 'is-invalid ' : ''}form-control`),
                      }}
                      messages={{
                        isRequired: messages.required,
                      }}
                    />
                    <small className="form-text d-block text-muted">Insert the name of your shop</small>
                  </div>

                  <div className="col-md-6">
                    <InputTextField
                      model=".vat"
                      name="vat"
                      label="Shop Code"
                      length={11}
                      validators={{
                        isRequired,
                        isVAT,
                      }}
                      mapProps={{
                        className: ({ fieldValue }) => (`${fieldValue.focus ? 'focus' : ''} ${!fieldValue.valid && !fieldValue.focus && !fieldValue.pristine ? 'is-invalid ' : ''}form-control`),
                      }}
                      messages={{
                        isRequired: messages.required,
                        isVAT: messages.shopCodeInvalid,
                      }}
                    />
                    <small className="form-text d-block text-muted">Insert the 11 digits VAT of your shop</small>
                  </div>

                  <div className="col-md-6">
                    <SelectTextField
                      label="Shop Type"
                      value={props.sector}
                      model=".businessSector"
                      name="businessSector"
                      validators={{
                        chooseCity,
                      }}
                      mapProps={{
                        className: ({ fieldValue }) => (`${fieldValue.focus ? 'focus' : ''} ${!fieldValue.valid && !fieldValue.focus && !fieldValue.pristine ? 'is-invalid ' : ''}form-control`),
                      }}
                      messages={{
                        isRequired: messages.required,
                      }}
                      data={category}
                    />
                    <small className="form-text d-block text-muted">Choose what the type of your shop is</small>
                  </div>

                  <div className="col-md-6">
                    <label className="fs-6 pb-2" htmlFor="numberOfEmployees">Shop Range</label>
                    <Control.select
                      model=".numberOfEmployees"
                      name="numberOfEmployees"
                      validators={{
                        isRequired,
                      }}
                      mapProps={{
                        className: ({ fieldValue }) => (`${fieldValue.focus ? 'focus' : ''} ${!fieldValue.valid && !fieldValue.focus && !fieldValue.pristine ? 'is-invalid ' : ''}form-control`),
                      }}
                      validateOn="change"
                    >
                      <option value="">Select a Range</option>
                      <option value="1-50">{'Small (< 50)'}</option>
                      <option value="50-250">Medium (Between 50 and 250)</option>
                      <option value="250+">{'Big (> 250)'}</option>
                    </Control.select>
                    <Errors
                      show={{ touched: true, focus: false, pristine: false }}
                      model={`${props.modelForm}.numberOfEmployees`}
                      className="invalid-feedback"
                      messages={{
                        isRequired: messages.required,
                      }}
                    />
                    <small className="form-text d-block text-muted">Insert a range of employees for your shop</small>
                  </div>

                  <div className="mb-3">
                    <h4>Registration address</h4>
                  </div>

                  <div className="col-md-6">
                    <SelectTextField
                      model=".city"
                      name="city"
                      label="City"
                      value={props.city}
                      validators={{
                        chooseCity,
                        handleCities,
                      }}
                      mapProps={{
                        className: ({ fieldValue }) => (`${fieldValue.focus ? 'focus' : ''} ${!fieldValue.valid && !fieldValue.focus && !fieldValue.pristine ? 'is-invalid ' : ''}form-control`),
                      }}
                      messages={{
                        handleCities: messages.cityInvalid,
                        chooseCity: messages.cityRequired,
                      }}
                      data={options}
                    />
                    <small className="form-text d-block text-muted">Insert the city where your shop is</small>

                  </div>

                  <div className="col-md-6">
                    <SelectTextFieldCounty
                      model=".county"
                      label="County"
                      value={props.county}
                      validators={{
                        isRequired,
                        isProvince,
                      }}
                      mapProps={{
                        className: ({ fieldValue }) => (`${fieldValue.focus ? 'focus' : ''} ${!fieldValue.valid && !fieldValue.focus && !fieldValue.pristine ? 'is-invalid ' : ''}form-control`),
                      }}
                      length={2}
                      data={counties}
                      messages={{
                        isRequired: messages.required,
                        isName: messages.nameInvalid,
                        isProvince: messages.provinceInvalid,
                      }}
                    />
                    <small className="form-text d-block text-muted">Insert the province where your shop is</small>

                  </div>

                  <div className="col-md-6">
                    <InputTextField
                      model=".streetWithNumber"
                      name="streetWithNumber"
                      label="Route"
                      validators={{
                        isRequired,
                      }}
                      mapProps={{
                        className: ({ fieldValue }) => (`${fieldValue.focus ? 'focus' : ''} ${!fieldValue.valid && !fieldValue.focus && !fieldValue.pristine ? 'is-invalid ' : ''}form-control`),
                      }}
                      messages={{
                        isRequired: messages.required,
                      }}
                    />
                    <small className="form-text d-block text-muted">Insert the address where your shop is</small>

                  </div>

                  <div className="col-md-6">
                    <InputTextField
                      model=".zipCode"
                      name="zipCode"
                      label="Postal Code"
                      validators={{
                        isRequired,
                        isPostalCode,
                      }}
                      mapProps={{
                        className: ({ fieldValue }) => (`${fieldValue.focus ? 'focus' : ''} ${!fieldValue.valid && !fieldValue.focus && !fieldValue.pristine ? 'is-invalid ' : ''}form-control`),
                      }}
                      length={5}
                      messages={{
                        isRequired: messages.required,
                        isPostalCode: messages.shopPostalCodeInvalid,
                      }}
                    />
                    <small className="form-text d-block text-muted">Insert the 5 digit postal code where your shop is</small>
                  </div>

                  <div className="fields pt-4 mt-1 mb-5 d-flex justify-content-around">
                    <button
                      className="btn btn-primary"
                      type="button"
                      onClick={handleBack}
                    >
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-left" viewBox="0 0 16 16">
                        <path fillRule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z" />
                      </svg>
                      Back
                    </button>
                    <button
                      className="btn btn-primary"
                      type="submit"
                      disabled={!props.isValid}
                    >
                      Next
                    </button>
                  </div>

                </Form>
              </div>
              <div className="card-footer text-center">
                <div className="small">
                  <div className="row justify-content-center">
                    {/*                     {modal && (
                      <ConfirmModal
                        onDeny={setModalFalseHandler}
                        title="Are you sure?"
                        message="Do you want to stop the form compilation? This will delete all the progress you made so far."
                        buttonDeny="Cancel"
                        buttonConfirm="Confirm"
                      />
                    )} */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};


RegistrationShopData.propTypes = {
  // Add here some propTypes
  actions: PropTypes.object.isRequired,
  modelForm: PropTypes.string.isRequired,
  isValid: PropTypes.bool,
  sector: PropTypes.string.isRequired,
  city: PropTypes.string.isRequired,
  county: PropTypes.string.isRequired,
};

RegistrationShopData.defaultProps = {
  // Add here some default propTypes values
  isValid: false,
};

export default RegistrationShopData;
