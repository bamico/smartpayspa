import { connect } from 'react-redux';
import { setCurrentModel } from '@store/actions/errorForm';
import { registrationNextStep, registrationPreviousStep } from '../../modules/RegistrationActions';
import RegistrationShopData from './RegistrationShopData';

const FORM_MODEL = 'registrationForm.RegistrationShopDataForm';

const mapDispatchToProps = dispatch => ({
  nextStep: () => { dispatch(registrationNextStep('SHOP_DATA')); },
  prevStep: () => { dispatch(registrationPreviousStep('SHOP_DATA')); },
  setCurrentModel: () => {
    dispatch(setCurrentModel(FORM_MODEL));
  },
});

const mapStateToProps = state => ({
  isValid: state.registrationForm.forms.RegistrationShopDataForm.$form.valid,
  city: state.registrationForm.RegistrationShopDataForm.city,
  county: state.registrationForm.RegistrationShopDataForm.county,
  sector: state.registrationForm.RegistrationShopDataForm.businessSector,
  modelForm: FORM_MODEL,
});

export default connect(mapStateToProps, mapDispatchToProps, (stateProps, dispatchProps, ownProps) => (
  {
    ...stateProps,
    own: ownProps,
    actions: dispatchProps,
  }
))(RegistrationShopData);
