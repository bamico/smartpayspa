/**
 *
 * Header
 *
 * @version 1.0.0
 * @author
 * @since
 */

import React, { useEffect, useState } from 'react';
// import { Link } from 'react-router-dom';
import ConfirmHandler from '../../../../components/ConfirmModal';

const Header = () => {
  const [modalState, setModalState] = useState(false);

  useEffect(() => {
  }, [window.location.href]);

  const setModalFalse = () => {
    setModalState(false);
  };

  const setModalTrue = () => {
    setModalState(true);
  };

  const modalOnConfirm = () => {
    setModalState(false);
    window.location.href = '/';
  };

  // const redirectLogout = () => {
  //   window.location.href = '/';
  //   sessionStorage.removeItem('token');
  // };

  // const redirectLogin = () => {
  //   window.location.href = '/';
  //   sessionStorage.removeItem('token');
  // };

  // login
  // const rawToken = sessionStorage.getItem('token');

  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light">
        <div className="container-fluid">
          <a className="navbar-brand fw-bold" href="/">SmartPay</a>
          <div className="collapse navbar-collapse" id="navbarText">
            <span className="navbar-text mx-3">
              <button type="button" className="btn btn-link" onClick={setModalTrue}>Close</button>
            </span>
            {/* {rawToken !== null && (
              <span className="navbar-button">
                <button type="button" className="btn rounded-pill bg-primary text-white" onClick={redirectLogout}>Logout</button>
              </span>
            )}
            {rawToken === null && (
              <span className="navbar-button">
                <button type="button" className="btn rounded-pill bg-primary text-white" onClick={redirectLogin}>Login</button>
              </span>
            )} */}
            {/* <span className="navbar-button">
              {!props.buttonStatus && <button type="button" className="btn  rounded-pill bg-primary text-white" onClick={props.login}>Login</button>}
            </span>
            <span className="navabar-btn">
              {props.buttonStatus && <button type="button" className="btn  rounded-pill bg-danger text-white" onClick={props.logout}>Logout</button>}
            </span> */}
            {/* {props.buttonStatus
            && (
              <span className="navbar-text">
                <Link className="btn-link mx-5" to="/profile/info">Profile</Link>
              </span>
            )} */}
          </div>
        </div>
      </nav>
      {modalState && (
        <ConfirmHandler
          onConfirm={modalOnConfirm}
          onDeny={setModalFalse}
          title="Stop Compiling?"
          message="Are you sure you want to stop compiling the form?"
          buttonDeny="Cancel"
          buttonConfirm="Stop Compiling"
        />
      )}
    </>
  );
};

Header.propTypes = {
  // logout: PropTypes.func.isRequired,
  // buttonStatus: PropTypes.bool,
  // actions: PropTypes.object,
};

Header.defaultProps = {
  hideCloseButton: false,
  // buttonStatus: false,
  // actions: {},
};
export default Header;
