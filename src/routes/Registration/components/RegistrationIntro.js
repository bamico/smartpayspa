/**
 *
 * Logout
 *
 */
import React from 'react';
import { useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import RouteContainer from '@components/RouteContainer';
import Button from '@components/Button';

const RegistrationIntro = (props) => {
  const history = useNavigate();

  // let checkToken = JSON.parse(window.sessionStorage.getItem('token'));
  const checkToken = false;
  const navigate = () => {
    props.actions.nextStep();
    history('/registration/personal-data');

  /*     if (checkToken === null) {
      checkToken = 'sdasfew3-3423d3fqwef-af3qrew3-32r';
      window.sessionStorage.setItem('token', JSON.stringify({
        token: `${checkToken}`,
      }));
    } */
  };

  const redirect = () => {
    history('/');
  };

  return (
    <>
      <RouteContainer>
        <div className="title">
          <div className="col">
            <p className="h2">Register in 5 minutes.</p>
          </div>
          <div className="col">
            <p className="h5 m-4 pb-5">What do you need?</p>
          </div>
          <div className="row">
            <div className="col">
              <svg xmlns="http://www.w3.org/2000/svg" width="95" height="60" fill="#9596af" className="bi bi-shop" viewBox="0 0 16 16">
                <path d="M2.97 1.35A1 1 0 0 1 3.73 1h8.54a1 1 0 0 1 .76.35l2.609 3.044A1.5 1.5 0 0 1 16 5.37v.255a2.375 2.375 0 0 1-4.25 1.458A2.371 2.371 0 0 1 9.875 8 2.37 2.37 0 0 1 8 7.083 2.37 2.37 0 0 1 6.125 8a2.37 2.37 0 0 1-1.875-.917A2.375 2.375 0 0 1 0 5.625V5.37a1.5 1.5 0 0 1 .361-.976l2.61-3.045zm1.78 4.275a1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0 1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0 1.375 1.375 0 1 0 2.75 0V5.37a.5.5 0 0 0-.12-.325L12.27 2H3.73L1.12 5.045A.5.5 0 0 0 1 5.37v.255a1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0zM1.5 8.5A.5.5 0 0 1 2 9v6h1v-5a1 1 0 0 1 1-1h3a1 1 0 0 1 1 1v5h6V9a.5.5 0 0 1 1 0v6h.5a.5.5 0 0 1 0 1H.5a.5.5 0 0 1 0-1H1V9a.5.5 0 0 1 .5-.5zM4 15h3v-5H4v5zm5-5a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1h-2a1 1 0 0 1-1-1v-3zm3 0h-2v3h2v-3z" />
              </svg>
              <p>VAT number</p>
            </div>

            <div className="col">
              <svg xmlns="http://www.w3.org/2000/svg" width="95" height="60" fill="#9596af" className="bi bi-hdd-network" viewBox="0 0 16 16">
                <path d="M4.5 5a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1zM3 4.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0z" />
                <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v1a2 2 0 0 1-2 2H8.5v3a1.5 1.5 0 0 1 1.5 1.5h5.5a.5.5 0 0 1 0 1H10A1.5 1.5 0 0 1 8.5 14h-1A1.5 1.5 0 0 1 6 12.5H.5a.5.5 0 0 1 0-1H6A1.5 1.5 0 0 1 7.5 10V7H2a2 2 0 0 1-2-2V4zm1 0v1a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4a1 1 0 0 0-1-1H2a1 1 0 0 0-1 1zm6 7.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5z" />
              </svg>
              <p>Registration Code</p>
              <p className="small">It will be sent to you during the registration</p>
            </div>

            <div className="col">
              <svg xmlns="http://www.w3.org/2000/svg" width="95" height="60" fill="#9596af" className="bi bi-envelope" viewBox="0 0 16 16">
                <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4Zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2Zm13 2.383-4.708 2.825L15 11.105V5.383Zm-.034 6.876-5.64-3.471L8 9.583l-1.326-.795-5.64 3.47A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.741ZM1 11.105l4.708-2.897L1 5.383v5.722Z" />
              </svg>
              <p>Email</p>
            </div>

            <div className="col">
              <svg xmlns="http://www.w3.org/2000/svg" width="95" height="60" fill="#9596af" className="bi bi-phone" viewBox="0 0 16 16">
                <path d="M11 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h6zM5 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H5z" />
                <path d="M8 14a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
              </svg>
              <p>Phone</p>
            </div>
          </div>
          <div className="d-grid gap-2 col-4 mx-auto mt-5">
            {!!checkToken === true
              ? (
                <div>
                  <div className="alert alert-primary mt-3 d-flex align-items-center" role="alert">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" className="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Warning:">
                      <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
                    </svg>
                    You have already been registered
                  </div>
                  <Button type="button" class="btn btn-primary btn-lg" onclick={redirect}>Back</Button>
                </div>
              )
              : <Button type="button" class="btn-primary btn-lg" onclick={navigate}>Start</Button>
            }
          </div>
        </div>
      </RouteContainer>
    </>
  );
};

RegistrationIntro.propTypes = {
  // Add here some propTypes
  actions: PropTypes.object.isRequired,
};

RegistrationIntro.defaultProps = {
  // Add here some default propTypes values
};

export default RegistrationIntro;
