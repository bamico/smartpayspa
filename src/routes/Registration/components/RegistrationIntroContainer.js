import { connect } from 'react-redux';
import RegistrationIntro from './RegistrationIntro';
import { registrationNextStep } from '../modules/RegistrationActions';


const mapDispatchToProps = dispatch => ({
  nextStep: () => { dispatch(registrationNextStep('PERSONAL_DATA')); },
});

const mapStateToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps, (stateProps, dispatchProps, ownProps) => (
  {
    ...stateProps,
    router: ownProps,
    actions: dispatchProps,
  }
))(RegistrationIntro);
