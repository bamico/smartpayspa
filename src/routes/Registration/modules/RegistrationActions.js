// import { createRequestTypes, createAction } from '../../../store/actions/index';

export const actionType = {
  NEXT_STEP: 'NEXT_STEP',
  PREVIOUS_STEP: 'PREVIOUS_STEP',
  CLOSE_SIGNUP: 'CLOSE_SIGNUP',
};

export const registrationNextStep = payload => (
  { type: actionType.NEXT_STEP,
    payload,
  }
);

export const registrationPreviousStep = () => (
  { type: actionType.PREVIOUS_STEP,
    payload: '',
  }
);

export const closeAction = data => (
  { type: actionType.CLOSE_SIGNUP,
    payload: data,
  }
);
