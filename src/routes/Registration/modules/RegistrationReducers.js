import dotProp from 'dot-prop-immutable';

const ACTION_HANDLERS = {
  NEXT_STEP: (state, actions) => {
    let newState = state;
    newState = dotProp.set(newState, 'stepFlow', actions.payload);
    return newState;
  },
  PREVIOUS_STEP: (state, action) => dotProp.set(state, 'stepFlow', action.payload),
};

const initialState = {
  stepFlow: 'INTRO',
};


export default function registration(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
