import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Header from './components/Header/Header';
import RegistrationIntro from './components/RegistrationIntroContainer';
import RegistrationPersonalData from './components/RegistrationPersonalData/RegistrationPersonalDataContainer';
import RegistrationDocumentData from './components/RegistrationDocumentData/RegistrationDocumentDataContainer';
import RegistrationShopData from './components/RegistrationShopData/RegistrationShopDataContainer';
import RegistrationSecurityData from './components/RegistrationSecurityData/RegistrationSecurityDataContainer';
import RegistrationFormSent from './components/RegistrationFormSent/RegistrationFormSentContainer';
import RegistrationFormError from './components/RegistrationFormError/RegistrationFormErrorContainer';

const RegistrationRoot = () => (
  <Routes>

    <Route
      path="/intro"
      element={(
        <>
          <Header hideBackButton />
          <RegistrationIntro />
        </>
      )}
    />
    <Route
      path="/personal-data"
      element={(
        <>
          <Header />
          <RegistrationPersonalData />
        </>
      )}
    />
    <Route
      path="/document-data"
      element={(
        <>
          <Header />
          <RegistrationDocumentData />
        </>
      )}
    />
    <Route
      path="/shop-data"
      element={(
        <>
          <Header />
          <RegistrationShopData />
        </>
      )}
    />
    <Route
      path="/security-data"
      element={(
        <>
          <Header />
          <RegistrationSecurityData />
        </>
      )}
    />
    <Route
      path="/form-sent"
      element={(
        <>
          <Header hideBackButton />
          <RegistrationFormSent />
        </>
      )}
    />
    <Route
      path="/form-sent-error"
      element={(
        <>
          <Header hideBackButton />
          <RegistrationFormError />
        </>
      )}
    />

  </Routes>
);

export default RegistrationRoot;
