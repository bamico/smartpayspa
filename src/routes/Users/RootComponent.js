import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Users from './components/Users';

const UsersRoot = () => (
  <Routes>
    <Route path="/" element={<Users />} />
  </Routes>
);

export default UsersRoot;
