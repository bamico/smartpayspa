import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

function Search(props) {
  const { searchOptions } = props;
  const [searchKey, setSearchKey] = useState('none');
  const [keyWord, setKeyWord] = useState('');
  const { searchData } = props;
  useEffect(() => {
    searchData(searchKey, keyWord);
  });

  return (
    <>
      <select
        value={searchKey}
        onChange={e => setSearchKey(e.target.value)}
        name="search-key"
        id="search-key"
        className="form-select"
      >
        <option defaultValue="none" value="none">Select what to search for</option>
        {searchOptions.map(option => (<option value={option.value}>{option.label}</option>))}
      </select>
      <input
        type="search"
        id="search-input"
        className="search-input col-6"
        value={keyWord}
        onChange={e => setKeyWord(e.target.value)}
        placeholder="Enter keyword"
      />

    </>
  );
}

Search.propTypes = {
  searchOptions: PropTypes.array.isRequired,
  searchData: PropTypes.func.isRequired,
};
export default Search;
