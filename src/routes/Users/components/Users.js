import axios from 'axios';
import React from 'react';
import Pagination from './Pagination';
import Search from './Search';

export default class Users extends React.Component {
  constructor() {
    super();
    this.state = {
      allUsers: [],
      filteredUsers: [],
      currentPageUsers: [],
      searchKey: 'none',
      keyWord: '',
    };
    this.searchUsers = this.searchUsers.bind(this);
    this.sortUsers = this.sortUsers.bind(this);
  }


  componentDidMount() {
    // Function to censor sensible data
    function censorData(user) {
      function doesPrefixExist(string) {
        if (string.charAt(0) === '+') {
          return 7;
        }
        return 3;
      }

      function asteriskReplacer(string, charsToKeep) {
        let censoredString = string.substr(0, charsToKeep);

        for (let i = 0; i < string.length - (charsToKeep + 1); i += 1) {
          censoredString += '*';
        }
        censoredString += string.charAt(string.length - 1);
        return censoredString;
      }

      user.id = asteriskReplacer(user.id, 1);
      user.phone = asteriskReplacer(user.phone, doesPrefixExist(user.phone));

      return user;
    }

    // HTTP request
    axios.get('https://test-7b314-default-rtdb.firebaseio.com/users.json')
      .then((response) => {
        // Converts the API data from object of objects to array of objects
        const allUsersObject = response.data;
        if (allUsersObject === null) {
          console.log('There are no users in the API.');
        } else {
          const userKeys = Object.keys(allUsersObject);
          const allUsers = [];
          userKeys.forEach((key, index) => {
            allUsersObject[key].key = key;
            allUsersObject[key].userid = index;
            censorData(allUsersObject[key]);
            allUsers.push(allUsersObject[key]);
          });
          this.setState({ allUsers });
          this.searchUsers('none', '');
        }
      });
  }

  showCurrentPage = (currentPageArray) => {
    // currentPageArray is the input recieved from component Pagination
    if (JSON.stringify(this.state.currentPageUsers) !== JSON.stringify(currentPageArray)) {
      this.setState({ currentPageUsers: currentPageArray });
    }
  }

  searchUsers(key, keyWord) {
    function areEqual(array1, array2) {
      if (array1.length === array2.length) {
        return array1.every((element) => {
          if (array2.includes(element)) {
            return true;
          }

          return false;
        });
      }

      return false;
    }
    const allUsers = this.state.allUsers.slice();
    if (key !== 'none' && keyWord.length >= 3) {
      const filteredUsers = [];
      allUsers.forEach((user) => {
        if (user[key].toLowerCase().includes(keyWord.toLowerCase())) {
          filteredUsers.push(user);
        }
      });
      let updateFilteredUsers = false;
      if (this.state.searchKey !== key) {
        this.setState({ searchKey: key });
        updateFilteredUsers = true;
      }
      if (this.state.keyWord !== keyWord) {
        this.setState({ keyWord });
        updateFilteredUsers = true;
      }
      if (updateFilteredUsers) {
        this.setState({ filteredUsers });
      }
    } else {
      const filteredUsers = allUsers.slice();
      if (!areEqual(this.state.filteredUsers, filteredUsers)) {
        this.setState({ filteredUsers });
      }
    }
  }

  sortUsers(key, order) {
    console.log('Before Sort');
    console.log(this.state.filteredUsers);
    const rawFilteredUsers = [];
    this.state.filteredUsers.forEach((user) => {
      rawFilteredUsers.push(user);
    });
    const filteredUsers = rawFilteredUsers.slice().sort((a, b) => {
      if (typeof a[key] === 'string') {
        const nameA = a[key].toLowerCase();
        const nameB = b[key].toLowerCase();
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }

        // names must be equal
        return 0;
      }
      return a[key] - b[key];
    });
    if (order === false) filteredUsers.reverse();
    console.log('After sort');
    this.setState({ filteredUsers }, console.log(filteredUsers));
  }


  render() {
    return (
      <>

        <div className="container pt-5">
          <div className="row col-12">
            <div className="col-12 card-custom table-responsive">
              <div className="col-12 d-flex py-3 justify-content-end">
                <Search
                  searchOptions={
                    [
                      {
                        label: 'Name',
                        value: 'name',
                      },
                      {
                        label: 'Surname',
                        value: 'surname',
                      },
                      {
                        label: 'Email',
                        value: 'email',
                      },
                    ]
                  }
                  changeSearchKey={this.changeSearchKey}
                  searchData={this.searchUsers}
                />
              </div>
              <table className="table align-middle table-borderless table-striped table-hover">
                <thead className="">
                  <tr className="text-main">
                    {/* <th>
                      User ID
                      <button
                        type="button"
                        className="custom-arrow text-main"
                        onClick={() => this.sortUsers('userid', true)}
                      >
                        &#9650;

                      </button>
                      <button
                        type="button"
                        className="custom-arrow text-main"
                        onClick={() => this.sortUsers('userid', false)}
                      >
                        &#9660;

                      </button>
                    </th>
                    <th>User Key</th> */}
                    <th>
                      Name
                      <button
                        type="button"
                        className="custom-arrow text-main"
                        onClick={() => this.sortUsers('name', true)}
                      >
                        &#9650;

                      </button>
                      <button
                        type="button"
                        className="custom-arrow text-main"
                        onClick={() => this.sortUsers('name', false)}
                      >
                        &#9660;

                      </button>

                    </th>
                    <th>
                      Surname
                      <button
                        type="button"
                        className="custom-arrow text-main"
                        onClick={() => this.sortUsers('surname', true)}
                      >
                        &#9650;

                      </button>
                      <button
                        type="button"
                        className="custom-arrow text-main"
                        onClick={() => this.sortUsers('surname', false)}
                      >
                        &#9660;

                      </button>

                    </th>
                    <th>
                      Phone
                      <button
                        type="button"
                        className="custom-arrow text-main"
                        onClick={() => this.sortUsers('phone', true)}
                      >
                        &#9650;

                      </button>
                      <button
                        type="button"
                        className="custom-arrow text-main"
                        onClick={() => this.sortUsers('phone', false)}
                      >
                        &#9660;

                      </button>

                    </th>
                    <th>
                      Fiscal Code
                      <button
                        type="button"
                        className="custom-arrow text-main"
                        onClick={() => this.sortUsers('id', true)}
                      >
                        &#9650;

                      </button>
                      <button
                        type="button"
                        className="custom-arrow text-main"
                        onClick={() => this.sortUsers('id', false)}
                      >
                        &#9660;

                      </button>
                    </th>
                    <th>
                      Email
                      <button
                        type="button"
                        className="custom-arrow text-main"
                        onClick={() => this.sortUsers('email', true)}
                      >
                        &#9650;

                      </button>
                      <button
                        type="button"
                        className="custom-arrow text-main"
                        onClick={() => this.sortUsers('email', false)}
                      >
                        &#9660;

                      </button>
                    </th>
                    <th>Details</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.currentPageUsers.length === 0
                    ? (
                      <tr>
                        <td colSpan={6}><p className="no-users-found"><i>No users found.</i></p></td>
                      </tr>
                    )
                    : null}
                  {this.state.currentPageUsers.map((user, index) => (
                    <>

                      <tr>
                        {/* <td>
                          #
                          {user.userid}
                        </td>
                        <td>

                          {user.key}
                        </td> */}
                        <td>{user.name}</td>
                        <td>{user.surname}</td>
                        <td>{user.phone}</td>
                        <td>{user.id}</td>
                        <td>{Object.prototype.hasOwnProperty.call(user, 'email') ? user.email : 'N/A'}</td>
                        <td>
                          <button
                            type="button"
                            className="btn btn-primary"
                            data-bs-toggle="collapse"
                            data-bs-target={`#user-${index}`}
                            aria-expanded="false"
                            aria-controls={`user-${ index}`}
                            id={`user-button-${ index}`}
                          >
                            Show details
                          </button>
                        </td>

                      </tr>
                      <tr>
                        <td colSpan="6">
                          <div className="collapse" id={`user-${index}`}>
                            <ul className="document-data d-flex justify-content-around align-items-center">
                              <li>{user.docType}</li>
                              <li>{user.docId}</li>
                              <li>
                                {`Expires on ${user.docExpire}`}
                              </li>
                            </ul>
                          </div>
                        </td>
                      </tr>
                    </>
                  ))}
                </tbody>
              </table>

              <Pagination
                arrayData={this.state.filteredUsers}
                elementsPerPage={7}
                showCurrentPage={this.showCurrentPage}
              />
            </div>
          </div>
        </div>
      </>
    );
  }
}
