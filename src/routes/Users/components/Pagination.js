import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

function Pagination(props) {
  const { arrayData } = props;
  const { elementsPerPage } = props;
  const { showCurrentPage } = props;
  const [currentPage, setCurrentPage] = useState(1);
  const currentPageData = arrayData.slice((currentPage - 1) * elementsPerPage, currentPage * elementsPerPage);
  useEffect(() => {
    // Sends array of current page elements to parent component
    showCurrentPage(currentPageData);
  });

  return (
    <div className="container pt-3">
      <div className="row">
        <div className="col-12 input-group justify-content-center">
          <ul className="paginator pagination pagination-lg justify-content-center">

            {currentPage !== 1
              ? (
                <>
                  <li className="page-item">
                    <button type="button" className="page-link" onClick={() => setCurrentPage(1)}>First</button>
                  </li>
                  <li className="page-item">
                    <button
                      type="button"
                      className="page-link"
                      onClick={() => setCurrentPage(currentPage - 1)}
                    >
                      Previous

                    </button>
                  </li>
                </>
              )
              : (
                <>
                  <li className="page-item disabled">
                    <button type="button" className="page-link" onClick={() => setCurrentPage(1)}>First</button>
                  </li>
                  <li className="page-item disabled">
                    <button
                      type="button"
                      className="page-link"
                      onClick={() => setCurrentPage(currentPage - 1)}
                    >
                      Previous

                    </button>
                  </li>
                </>
              )
            }

            <li className="page-item">
              {currentPage === Math.ceil(arrayData.length / elementsPerPage) ? (
                <button
                  type="button"
                  className="page-link"
                  onClick={() => setCurrentPage(currentPage - 2)}
                >
                  <p className="p-pagination-custom">{currentPage - 2}</p>

                </button>
              ) : null}
            </li>
            <li className="page-item">
              {currentPage !== 1 ? (
                <button
                  type="button"
                  className="page-link"
                  onClick={() => setCurrentPage(currentPage - 1)}
                >
                  <p className="p-pagination-custom">{currentPage - 1}</p>

                </button>
              ) : null}

            </li>
            <li className="page-item active">
              <button type="button" className="page-link">
                <p className="p-pagination-custom">{currentPage}</p>
              </button>
            </li>
            <li className="page-item">
              {currentPage !== Math.ceil(arrayData.length / elementsPerPage) ? (
                <button
                  type="button"
                  className="page-link"
                  onClick={() => setCurrentPage(currentPage + 1)}
                >
                  <p className="p-pagination-custom">{currentPage + 1}</p>

                </button>
              ) : null}
            </li>
            <li className="page-item">
              {currentPage === 1 ? (
                <button
                  type="button"
                  className="page-link"
                  onClick={() => setCurrentPage(currentPage + 2)}
                >
                  <p className="p-pagination-custom">{currentPage + 2}</p>

                </button>
              ) : null}

            </li>
            {currentPage !== Math.ceil(arrayData.length / elementsPerPage)
              ? (
                <>
                  <li className="page-item">
                    <button
                      type="button"
                      className="page-link"
                      onClick={() => setCurrentPage(currentPage + 1)}
                    >
                      {' '}
                      Next

                    </button>
                  </li>
                  <li className="page-item">
                    <button
                      type="button"
                      className="page-link"
                      onClick={() => setCurrentPage(Math.ceil(arrayData.length / elementsPerPage))}
                    >
                      Last
                    </button>
                  </li>
                </>
              )
              : (
                <>
                  <li className="page-item disabled">
                    <button
                      type="button"
                      className="page-link"
                      onClick={() => setCurrentPage(currentPage + 1)}
                    >
                      {' '}
                      Next

                    </button>
                  </li>
                  <li className="page-item disabled">
                    <button
                      type="button"
                      className="page-link"
                      onClick={() => setCurrentPage(Math.ceil(arrayData.length / elementsPerPage))}
                    >
                      Last
                    </button>
                  </li>
                </>
              )
            }

          </ul>
        </div>
      </div>
    </div>
  );
}

Pagination.propTypes = {
  arrayData: PropTypes.array.isRequired,
  elementsPerPage: PropTypes.number,
  showCurrentPage: PropTypes.func.isRequired,
};
Pagination.defaultProps = {
  elementsPerPage: 10,
};

export default Pagination;
