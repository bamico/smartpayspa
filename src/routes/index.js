// We only need to import the modules necessary for initial render
// When you add a first route be sure to remove the comment from the lines below.

import React, { lazy } from 'react';
import { Route } from 'react-router-dom';
// import asyncComponent from '../utils/AsyncComponent';
import LazyComponent from '../utils/LazyComponent';

const Login = lazy(() => import('./Login'));
const Registration = lazy(() => import('./Registration'));
const Users = lazy(() => import('./Users'));
const Profile = lazy(() => import('./Profile'));
const ForgotPassword = lazy(() => import('./ForgotPassword'));

export const Components = {
  Login,
  Registration,
  Users,
  Profile,
  ForgotPassword,
};

export const createAuthRoutes = () => ({
  routes: [

  ],
});

// These routes don't need the user to be logged in.

export const createUnauthRoutes = () => ({
  routes: [
    {
      component: () => <Route path="/*" exact key="Login" element={<LazyComponent componentName="Login" />} />,
    },
    {
      component: () => <Route path="/registration/*" exact key="Registration" element={<LazyComponent componentName="Registration" />} />,
    },
    {
      component: () => <Route path="/users/*" key="Users" element={<LazyComponent componentName="Users" />} />,
    },
    {
      component: () => <Route path="/profile/*" exact key="Profile" element={<LazyComponent componentName="Profile" />} />,
    },
    {
      component: () => <Route path="/forgot-password/*" exact key="ForgotPassword" element={<LazyComponent componentName="ForgotPassword" />} />,
    },
  ],
});

export default createAuthRoutes;
