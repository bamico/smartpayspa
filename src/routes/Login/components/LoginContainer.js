import { connect } from 'react-redux';
import { backEndError, setCurrentModel } from '@store/actions/errorForm';
import { LoginNextStep } from '../modules/LoginActions';
import Login from './Login';

const FORM_MODEL = 'registrationForm.Login';

const mapDispatchToProps = dispatch => ({
  nextStep: () => { dispatch(LoginNextStep('LOGIN')); },
  setCurrentModel: () => {
    dispatch(setCurrentModel(FORM_MODEL));
  },
  backEndError: (errors) => { dispatch(backEndError(errors)); },
});

const mapStateToProps = state => ({
  myprop: state.loginForm,
});

export default connect(mapStateToProps, mapDispatchToProps, (stateProps, dispatchProps, ownProps) => (
  {
    ...stateProps,
    own: ownProps,
    actions: dispatchProps,
  }
))(Login);
