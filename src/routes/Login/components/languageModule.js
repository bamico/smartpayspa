import React from 'react';
import { FormattedMessage } from 'react-intl';

const formattedMessage = {
  required:
  <FormattedMessage
    id="smartpay.registration.error.required"
    defaultMessage="Field required"
  />,
  emailInvalid:
  <FormattedMessage
    id="smartpay.registration.error.email"
    defaultMessage="email address format not valid"
  />,
  pwdInvalid:
  <FormattedMessage
    id="smartpay.registration.error.pwd"
    defaultMessage="password format not valid"
  />,
};

export default formattedMessage;
