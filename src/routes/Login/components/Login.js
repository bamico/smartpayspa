/**
 *
 * Logout
 *
 */
import React, { useRef, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import loginData from '@api/login/login';
// import ErrorCode from '../../../components/ErrorCode';
// import Button from '@components/Button';
// import PropTypes from 'prop-types';
import ReCAPTCHA from 'react-google-recaptcha';

const eye = (
  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="black" className="bi bi-eye" viewBox="0 0 16 16">
    <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z" />
    <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z" />
  </svg>
);

const eyeSlash = (
  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="black" className="bi bi-eye-slash" viewBox="0 0 16 16">
    <path d="M13.359 11.238C15.06 9.72 16 8 16 8s-3-5.5-8-5.5a7.028 7.028 0 0 0-2.79.588l.77.771A5.944 5.944 0 0 1 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.134 13.134 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755-.165.165-.337.328-.517.486l.708.709z" />
    <path d="M11.297 9.176a3.5 3.5 0 0 0-4.474-4.474l.823.823a2.5 2.5 0 0 1 2.829 2.829l.822.822zm-2.943 1.299.822.822a3.5 3.5 0 0 1-4.474-4.474l.823.823a2.5 2.5 0 0 0 2.829 2.829z" />
    <path d="M3.35 5.47c-.18.16-.353.322-.518.487A13.134 13.134 0 0 0 1.172 8l.195.288c.335.48.83 1.12 1.465 1.755C4.121 11.332 5.881 12.5 8 12.5c.716 0 1.39-.133 2.02-.36l.77.772A7.029 7.029 0 0 1 8 13.5C3 13.5 0 8 0 8s.939-1.721 2.641-3.238l.708.709zm10.296 8.884-12-12 .708-.708 12 12-.708.708z" />
  </svg>
);

let icon = eye;

const Login = (props) => {
  // console.log('***');

  const navigate = useNavigate();

  const inputEmailRef = useRef('');
  const inputPasswordRef = useRef('');
  const captchaRef = useRef('');

  const [errorInputPassword, setErrorInputPassword] = useState('');
  const [errorInputEmail, setErrorInputEmail] = useState('');

  const [errorFieldInputEmail, setErrorFieldInputEmail] = useState(false);
  const [errorFieldInputPassword, setErrorFieldInputPassword] = useState(false);

  const [errorMessage, setErrorMessage] = useState(false);
  const [submitted, setSubmitted] = useState(false);

  // states required true to abilitate the CTA
  const [emailCheck, setEmailCheck] = useState(false);
  const [pwdCheck, setPwdCheck] = useState(false);
  const [captchaCheck, setCaptchaCheck] = useState(false);

  // Show Hide Password
  const [showPassword, setShowPassword] = useState(false);

  const togglePassword = () => {
    setShowPassword(!showPassword);

    if (showPassword) {
      icon = eye;
    } else {
      icon = eyeSlash;
    }
  };

  // console.log(captchaCheck);

  const handleCaptcha = () => {
    if (pwdCheck && emailCheck) {
      setCaptchaCheck(true);
    }
  };

  const reCaptchaCheck = () => {
    const recaptchaValue = captchaRef.current.getValue();
    console.debug('Captcha value:', recaptchaValue);

    handleCaptcha();
  };

  const handleExpired = () => {
    setCaptchaCheck(false);
    // console.log(captchaCheck);
  };


  /*
  const database = [
    {
      email: 'user1@email.com',
      password: 'Password1!',
    },
    {
      email: 'user2@email.com',
      password: 'Password2!',
    },
  ];
  */


  /* blocks input field after user insert wrong password more than 5 times
  const handleBlockedUser = () => {
    setCaptchaCheck(false);
    setEmailCheck(false);
    setPwdCheck(false);

    inputEmailRef.current.disabled = true;
    inputPasswordRef.current.disabled = true;
    captchaRef.current.disabled = true;
  }; */

  const handleSubmit = (event) => {
    event.preventDefault();
    // console.log('HandleSubmit is being called (:');

    const inputData = {
      email: inputEmailRef.current.value,
      password: inputPasswordRef.current.value,
    };

    console.log('submitted:', submitted);
    // console.log(inputData);

    // Find user's email login info
    // const userData = database.find(user => user.email === inputEmailRef.current.value);

    /* Compare data with stored info
    if (userData) {
      if (userData.password !== inputPasswordRef.current.value) {
        setErrorMessage('Wrong password');
        userAttempts += 1;
        console.log(userAttempts);
        // After 5 attempts user gets blocked
        if (userAttempts > 4) {
          setErrorMessage('Too many wrong attempts to log-in. Please try again in 24 hours');
          handleBlockedUser();
        }
      } else {
        setSubmitted(true);
        console.log('Success!');
        setErrorMessage('');


      }
    } else {
      setErrorMessage('Email not registered');
    } */

    // Test for non-VPN
    // setErrorMessage('Wrong email or password');
    // navigate('/user-info');

    /* if (userAttempts > 4) {
      setErrorMessage('Too many wrong attempts to log-in. Please try again in 24 hours');
      handleBlockedUser();
    } */

    // POST call
    const formData = new FormData();
    formData.append('inputData', {
      type: 'application/json',
    });
    loginData(inputData).then((r) => {
      console.log('next step');
      const response = r;
      window.prova = r;
      if ((response.error !== undefined && response.error.status >= 500) || response.response === undefined) {
        console.log('error >= 500');
        setErrorMessage('Something went wrong. Please try again later');
      } else if (r.error !== undefined && r.error.status === 401) {
        console.log('error 401');
        props.actions.backEndError(r.error.data.errors);
      } else if (r.error !== undefined && r.error.status === 400) {
        console.log('error 400');
        props.actions.backEndError(r.error.data.errors);
      } else if (r.response.status !== undefined && r.response.status === 200) {
        setSubmitted(true);
        console.log('Success!');
        setErrorMessage('');
        console.log(r.response.data);
        sessionStorage.setItem('token', r.response.data.token);
        navigate('/profile/info');
        props.actions.backEndError();
      }
      console.log(r);
    }).catch((e) => {
      console.log('FAILURE!! ', e);
    });
  };


  const validateEmail = () => {
    console.log('validate email');

    captchaRef.current.reset();
    setCaptchaCheck(false);

    const emailValue = inputEmailRef.current.value;

    if (emailValue.length < 1) {
      setErrorInputEmail('Required');
      setErrorFieldInputEmail(true);
    } else {
      setErrorInputEmail('');
      setErrorFieldInputEmail(false);
      setEmailCheck(true);
    }
    if (!/^(([^<>()\[\]\\.,;:òàèìù\s@"']+(\.[^<>()\[\]\\.,;:àèìòù\s@"']+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(emailValue)) {
      setErrorFieldInputEmail(true);
      setErrorInputEmail('Email format is not valid');
    } else {
      setErrorFieldInputEmail(false);
      setErrorInputEmail('');
      setPwdCheck(true);
    }
  };

  const validatePassword = () => {
    console.log('validate pwd');

    captchaRef.current.reset();
    setCaptchaCheck(false);

    const passwordValue = inputPasswordRef.current.value;

    if (passwordValue.length < 1) {
      setErrorInputPassword('Required');
      setErrorFieldInputPassword(true);
    } else {
      setErrorInputPassword('');
      setErrorFieldInputPassword(false);
    }
  };

  // const error = {
  //   number: 10001,
  //   description: 'Required field is empty',
  // };

  const removeError = () => {
    props.actions.backEndError();
  };

  return (
    <>
      {/* <ErrorCode
        error={error}
      /> */}
      <header className="header is-fixed header--full header-light header-login login--private">
        <div className="header-top nx-container-padded">
          <div className="justify-content-between align-items-center d-flex animated fadeIn">
            <div className="header-logo">
              <a href="/" title="Smart Pay">
                <h5 className="h1 text-white">Smart Pay</h5>
              </a>
            </div>
          </div>
        </div>
        <div className="header-menu-expand nx-container-padded" />
      </header>
      <div className="login-container">
        <div className="login">
          <div className="container px-5">
            <div className="row gx-5 align-items-center">
              <div className="col-lg-6 mb-5 mb-lg-0 text-center text-lg-start">
                <h1 className="display-1 lh-1 mb-3" style={{ textShadow: '2px 2px #00000F' }}>Welcome to SmartPay</h1>
                <p className="lead fw-normal mb-5" style={{ textShadow: '2px 2px #00000F' }}>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Quod, odio recusandae. Reiciendis omnis unde maiores vitae, possimus aliquam corporis dignissimos?
                  A repudiandae eaque alias voluptatum voluptates animi sint obcaecati doloremque.
                </p>
              </div>
              <div className="col-lg-6">
                <div className="login-form">
                  <div className="main-div">
                    <div className="panel">
                      <p className="login-title">Personal Area</p>
                      <p className="login-subtitle">Personal area is reserved for merchant users</p>
                    </div>

                    <form id="Login" name="form" noValidate>
                      <div className="form-group">
                        <input
                          type="text"
                          className={errorFieldInputEmail ? 'form-control is-invalid' : 'form-control'}
                          id="inputEmail"
                          placeholder="Email Address"
                          onChange={() => validateEmail()}
                          ref={inputEmailRef}
                        />
                        <div className={errorInputEmail ? 'error' : 'margin-error'} id="email-error">{errorInputEmail}</div>
                      </div>
                      <div className="form-group">
                        <input
                          type={showPassword ? 'text' : 'password'}
                          className={errorFieldInputPassword ? 'form-control is-invalid' : 'form-control'}
                          id="inputPassword"
                          placeholder="Password"
                          onChange={() => validatePassword()}
                          ref={inputPasswordRef}
                        />

                        {/* <i onClick={togglePassword} className={`bi ${showPassword ? 'bi-eye-slash' : 'bi-eye'}`} /> */}

                        <span
                          onClick={togglePassword}
                          style={{
                            float: 'right',
                            padding: '10px',
                          }}
                        >
                          {icon}
                        </span>

                        <div className={errorInputPassword ? 'error' : 'margin-error'} id="password-error">{errorInputPassword}</div>
                        <div className="lin-recovery" id="recoveryintro" onClick={removeError}>
                          <Link to="/forgot-password/" className="recover-lnk">
                            Forgot password?
                          </Link>
                        </div>
                      </div>
                      <div className="action">
                        {/* <div className="remember-box">
                          <input type="checkbox" id="user-remember" value="remember" className="grommetux-check-box__control" />
                          <label htmlFor="user-remember">Remember my email</label>
                        </div> */}

                        {/*
                        <div className="remember-box">
                          <input
                            type="checkbox"
                            id="captcha"
                            name="captcha"
                            value="check"
                            className="grommetux-check-box__control"
                            onChange={handleCaptcha}
                            checked={captchaCheck}
                            ref={captchaRef}
                          />
                          <label htmlFor="captcha-placeholder"><p>reCAPTCHA Placeholder</p></label>
                        </div>
                        */}
                        <br />
                        <div className="gRecaptcha">
                          <ReCAPTCHA
                            sitekey="6Lcmvw4fAAAAAAx8Jeph5pLdK-9K1w3uV2lt0FJ9"
                            // sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI" // test
                            onChange={() => reCaptchaCheck()}
                            onExpired={() => handleExpired()}
                            ref={captchaRef}
                          />
                        </div>

                        <span className="error" id="submit-error"><h4>{errorMessage}</h4></span>
                        <button
                          type="button"
                          className="btn btn-primary btn-block"
                          disabled={!(emailCheck, pwdCheck, captchaCheck)}
                          onClick={handleSubmit}
                        >
                          Log in
                        </button>

                      </div>
                    </form>
                    <div className="login-banner__text">
                      <p>Do you own a shop and want to manage your POS online?</p>
                    </div>
                    <div className="row">
                      <div className="col">
                        <Link to="/registration/intro">
                          <span className="link-cta">SIGN UP</span>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer id="footer-lite">
        <div className="footer-wrapper">
          <div className="footer-wrapper-logo">
            <h5 className="h1">Smart Pay</h5>
          </div>
          <div className="footer-text">
            <a href="https://www.nexi.it/privacy.html" className="footer-privacy-text at-element-marker" target="_blank" rel="noreferrer">Privacy</a>
            <span className="footer-copyright-text">© Atlas Reply S.r.l. 2022. All Rights Reserved.</span>
          </div>
        </div>
      </footer>
    </>
  );
};


Login.propTypes = {
  // Add here some propTypes
  // modelForm: PropTypes.string.isRequired,
  actions: PropTypes.object.isRequired,
};

Login.defaultProps = {
  // Add here some default propTypes values
};

export default Login;
