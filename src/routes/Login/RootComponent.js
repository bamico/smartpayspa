import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Login from './components/LoginContainer';

const LoginRoot = () => (
  <Routes>
    <Route
      path="/"
      element={(
        <>
          <Login />
        </>
      )}
    />
  </Routes>
);

export default LoginRoot;
