import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Footer from '../../components/Footer/Footer';
import ForgotPassword from './components/ForgotPassword/ForgotPasswordContainer';

const LoginRoot = () => (
  <Routes>
    <Route
      path="/"
      element={(
        <>
          <ForgotPassword />
          <Footer />
        </>
      )}
    />
  </Routes>
);

export default LoginRoot;
