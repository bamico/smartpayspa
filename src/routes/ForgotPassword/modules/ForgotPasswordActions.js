// import { createRequestTypes, createAction } from '../../../store/actions/index';

export const actionType = {
  SET_TOKEN: 'SET_TOKEN',
  DELETE_TOKEN: 'DELETE_TOKEN',
};

export const setToken = payload => (
  { type: actionType.SET_TOKEN,
    payload,
  }
);

export const deleteToken = payload => (
  { type: actionType.DELETE_TOKEN,
    payload,
  }
);
