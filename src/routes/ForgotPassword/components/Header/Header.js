/**
 *
 * Header
 *
 * @version 1.0.0
 * @author
 * @since
 */

import React, { useState } from 'react';
import ConfirmHandler from '../../../../components/ConfirmModal';

const Header = () => {
  const [modalState, setModalState] = useState(false);

  const setModalFalse = () => {
    setModalState(false);
  };

  const modalOnConfirm = () => {
    setModalState(false);
    window.location.href = 'http://smartpay-02.nets.local:8080/';
  };

  // login

  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light">
        <div className="container-fluid">
          <a className="navbar-brand fw-bold" href="/">SmartPay</a>
        </div>
      </nav>
      {modalState && (
        <ConfirmHandler
          onConfirm={modalOnConfirm}
          onDeny={setModalFalse}
          title="Stop Compiling?"
          message="Are you sure you want to stop compiling the form?"
          buttonDeny="Cancel"
          buttonConfirm="Stop Compiling"
        />
      )}
    </>
  );
};

Header.propTypes = {
  // actions: PropTypes.object,
};

Header.defaultProps = {
  hideCloseButton: false,
  buttonStatus: false,
  // actions: {},
};
export default Header;
