import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import Header from '../Header/Header';
import { sendEmail, sendData } from '../../../../api/forgotPassword/forgotPassword';

const LoginProfileInfo = (props) => {
  // fake login
  const [button, setButton] = useState(false);
  const [callStatus, setCallStatus] = useState(1);
  const fieldRef = useRef();
  const fieldRef2 = useRef();
  const [field1Value, setField1Value] = useState('');
  const [field2Value, setField2Value] = useState('');
  const [regex1, setRegex1] = useState(false);
  const [regex2, setRegex2] = useState(false);
  const [error, setError] = useState('');
  const [data, setData] = useState({});


  const Login = () => {
    setButton(true);
    props.actions.setToken();
  };

  const Logout = () => {
    setButton(false);
    props.actions.deleteToken();
  };

  // First Call

  const testRegex = () => {
    if (/^(?=.{1,64}@)[A-Za-z0-9_-]+(\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,})$/i.test(fieldRef.current.value)) {
      setRegex1(true);
      setError('');
    } else {
      setError('Invalid Email');
      setRegex1(false);
    }
  };

  const getValue = () => {
    setField1Value(fieldRef.current.value);
    testRegex();
  };

  const sendEmailHandler = (event) => {
    event.preventDefault();
    sendEmail(field1Value).then((r) => {
      if (r.response === undefined) {
        setError('Something was wrong. Pleasy try again later');
      } else if (r.response.status === 200) {
        setData(r.response.data);
        setCallStatus(2);
      } else if (r.error.status === 404) {
        setError('This email is not registered');
      }
      // } else {
      //   setError('Something was wrong. Pleasy try again later');
      // }
    });
  };

  // Second Call

  const testRegex2 = () => {
    if (fieldRef2.current.value.length > 0) {
      setRegex2(true);
    } else {
      setRegex2(false);
    }
  };

  const getValue2 = () => {
    setField2Value(fieldRef2.current.value);
    testRegex2();
  };

  const sendDataHandler = (event) => {
    event.preventDefault();
    setField2Value(fieldRef2.current.value);
    const object = {
      email: field1Value,
      secQuestion: data.secQuestion,
      secQuestionAnswer: field2Value,
    };
    sendData(object).then((r) => {
      if (r.response === undefined) {
        setError('Something was wrong. Please try again later');
      } else if (r.response.status === 200) {
        setCallStatus(3);
      } else if (r.error.status === 400) {
        setError('Incorrect Answer');
      }
    });
  };


  return (
    <>
      <Header login={Login} logout={Logout} buttonStatus={button} />

      <div className="container">

        {callStatus === 1 && (
          <form className="my-5 border" onSubmit={sendEmailHandler}>
            <h2 className="my-4">Recover Password</h2>
            <p>Insert the email and answer the question you chose to create your account, and we&apos;ll send an email with the instructions to recover the password</p>
            <div className="row justify-content-center">
              <label className="col-12" htmlFor="E-mail">E-mail</label>
              <input className="form-control w-25" type="text" onChange={getValue} ref={fieldRef} />
              {field1Value !== '' && <span>{error}</span>}
            </div>
            <div className="row justify-content-center">
              <button disabled={!regex1} className="my-3 col-1 btn rounded-pill bg-primary text-white" type="submit">Submit</button>
            </div>
          </form>
        )}

        {callStatus === 2 && (
          <form className="my-5 border" onSubmit={sendDataHandler}>
            <h2 className="my-4">Recover Password</h2>
            <p>Insert the email and answer the question you chose to create your account, and we&apos;ll send an email with the instructions to recover the password</p>
            <div className="row justify-content-center">
              <label className="col-12" htmlFor="E-mail">{data.secQuestion}</label>
              <input className="form-control w-25" type="text" ref={fieldRef2} onChange={getValue2} />
              {field2Value !== '' && <span>{error}</span>}
            </div>
            <div className="row justify-content-center">
              <button disabled={!regex2} className="my-3 col-1 btn rounded-pill bg-primary text-white" type="submit">Submit</button>
            </div>
          </form>
        )}

        {callStatus === 3 && (
          <h2 className="p-5 my-5 border">You will shortly receive an email with a transative password to access your account. We advice you to change it as soon as you login correctly.</h2>
        )}

      </div>
    </>
  );
};


LoginProfileInfo.propTypes = {
  // Add here some propTypes
  actions: PropTypes.object.isRequired,
};

LoginProfileInfo.defaultProps = {
  // Add here some default propTypes values
};

export default LoginProfileInfo;
