import { connect } from 'react-redux';
// import { setCurrentModel } from '@store/actions/errorForm';
import ForgotPassword from './ForgotPassword';
import { deleteToken, setToken } from '../../modules/ForgotPasswordActions';

// const FORM_MODEL = 'forgotPasswordForm.ForgotPasswordEmail';

const mapDispatchToProps = dispatch => ({
  setToken: () => { dispatch(setToken()); },
  deleteToken: () => { dispatch(deleteToken()); },
  // setCurrentModel: () => {
  //   dispatch(setCurrentModel(FORM_MODEL));
  // },
});

const mapStateToProps = () => ({
  // isValid: state.forgotPasswordForm.forgotPassword.ForgotPasswordEmail.$form.valid,
  // modelForm: FORM_MODEL,
});

export default connect(mapStateToProps, mapDispatchToProps, (stateProps, dispatchProps, ownProps) => (
  {
    ...stateProps,
    own: ownProps,
    actions: dispatchProps,
  }
))(ForgotPassword);
