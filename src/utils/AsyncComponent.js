import React from 'react';
import PropTypes from 'prop-types';

export default function asyncComponent(getComponent) {
  class AsyncComponent extends React.Component {
    static propTypes = {
      history: PropTypes.object.isRequired,
    };

      state = { Component: AsyncComponent.Component };

      componentWillMount() {
        if (!this.state.Component) {
          getComponent().then((Component) => {
            AsyncComponent.Component = Component;
            this.setState({ Component });
          });
        }
      }


      render() {
        const { Component } = this.state;
        if (Component) {
          return <Component {...this.props} />;
        }
        return null;
      }
  }
  return AsyncComponent;
}
