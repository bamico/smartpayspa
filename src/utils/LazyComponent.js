import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import Loader from '@components/Loader';
import { Components } from '../routes';
import rootStore from '../store/createStore';
import { injectReducer } from '../store/reducers';
import { injectSaga } from '../store/sagas';

export const LazyComponent = (props) => {
  const { componentName } = props;
  import(`../routes/${componentName}/modules/${componentName}Reducers`)
    .then(({ default: reducer }) => {
      if (reducer !== undefined) {
        injectReducer(rootStore, { key: componentName, asyncReducer: reducer });
      }
    });
  import(`../routes/${componentName}/modules/${componentName}Sagas`)
    .then(({ default: saga }) => {
      if (saga !== undefined) {
        injectSaga(rootStore, { key: componentName, asyncSaga: saga });
      }
    });
  const Component = Components[componentName];
  return (
    <Suspense fallback={<Loader />}>
      <Component {...props} />
    </Suspense>
  );
};

LazyComponent.propTypes = {
  componentName: PropTypes.string.isRequired,
};

export default LazyComponent;
