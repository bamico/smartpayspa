/* eslint-disable no-bitwise */

/* eslint-disable arrow-body-style */
export const isRequired = val => val && val.length;
export const privacyCheck = val => !!val;
export const isName = val => /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,30}$/.test(val);
export const isSurname = val => /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{2,40}$/.test(val);
export const isEmail = val => /^(?=.{1,64}@)[A-Za-z0-9_-]+(\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,})$/i.test(val);
export const isMobilePhone = val => /^(\+\d{1,2}\s?)?1?\-?\.?\s?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/.test(val);
export const isFiscalCode = val => /^(?:[A-Z][AEIOU][AEIOUX]|[AEIOU]X{2}|[B-DF-HJ-NP-TV-Z]{2}[A-Z]){2}(?:[\dLMNP-V]{2}(?:[A-EHLMPR-T](?:[04LQ][1-9MNP-V]|[15MR][\dLMNP-V]|[26NS][0-8LMNP-U])|[DHPS][37PT][0L]|[ACELMRT][37PT][01LM]|[AC-EHLMPR-T][26NS][9V])|(?:[02468LNQSU][048LQU]|[13579MPRTV][26NS])B[26NS][9V])(?:[A-MZ][1-9MNP-V][\dLMNP-V]{2}|[A-M][0L](?:[1-9MNP-V][\dLMNP-V]|[0L][1-9MNP-V]))[A-Z]$/i.test(val);
export const confirmEmail = (vals) => {
  return vals.confirmEmail === '' || vals.email === vals.confirmEmail;
};
export const isPassword = val => /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[~!@#$%^&*_\-+=\\`|(){}[\]:;"'<>,.?/ ])[A-Za-z\d~!@#$%^&*_\-+=\\`|(){}[\]:;"'<>,.?/ ]{8,30}$/.test(val);
export const confirmPassword = (vals) => {
  return vals.confirmPassword === '' || vals.password === vals.confirmPassword;
};
export const isPasswordLongEnough = val => val && (val.length >= 8 || val.length === 0);
export const isPasswordLongTooMuch = val => val && val.length <= 20;
export const chooseCity = val => val !== '';
export const isVAT = val => /^(0)[0-9]{10}$/.test(val);
export const isPostalCode = val => /^[0-9]{5}$/.test(val);
export const isProvince = val => /^[a-zA-Z]{2}$/.test(val);
export const isAnswer = val => /^[A-Za-z]{5,30}$/.test(val);

const regExobject = {
  '': new RegExp(isRequired),
  passport: new RegExp('^[A-Za-z]{2}[0-9]{7}$'),
  'physical identity card': new RegExp('^[A-Za-z]{2}[0-9]{7}$'),
  /* 'physical identity card': new RegExp('^[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$'), */
  'electronic identity card': new RegExp('^[a-zA-Z]{2}[0-9]{5}[a-zA-Z]{2}$'),
  /* DriversLicense: new RegExp('^(([A-Z]{2}[0-9]{2})( )|([A-Z]{2}-[0-9]{2}))((19|20)[0-9][0-9])[0-9]{7}$') */
};
export const documentFormat = (vals) => {
  if (vals.id) {
    return regExobject[vals.type].test(vals.id);
  }
  return true;
};

export const isValidVAT = (val) => {
  // Normalize VAT
  const vat = val.replace(/\s/g, '');

  if (!/^[0-9]{11}$/.test(vat)) return false;

  let s = 0;
  for (let i = 0; i < 11; i += 1) {
    let n = vat.charCodeAt(i) - '0'.charCodeAt(0);
    if ((i & 1) === 1) {
      n *= 2;
      if (n > 9) n -= 9;
    }
    s += n;
  }
  if (s % 10 !== 0) return false;
  return null;
};
