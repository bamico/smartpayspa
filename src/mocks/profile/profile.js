import { mockInstance as mock } from '@api';

import getProfileData from './jsonmocks/profiles.GET';

mock.onGet(`/profiles/7`).reply(200, {
  ...getProfileData,
});

export default mock;
