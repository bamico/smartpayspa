import { mockInstance as mock } from '@api';

import getLoginData from './jsonmocks/login.GET';

mock.onGet(`/profiles/7`).reply(200, {
  ...getLoginData,
});

export default mock;
