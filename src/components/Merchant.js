import React from 'react';

const Merchant = () => {
  const ciao = 'ciao';

  return (
    <div className="my-5 container">
      {ciao}

      <h2 className="mt-5">Shop Details</h2>

      <table className="table table-hover">
        <thead>
          <tr>
            <th scope="col">Shop Name</th>
            <th scope="col">City</th>
            <th scope="col">Province</th>
            <th scope="col">Route</th>
            <th scope="col">CAP</th>
            <th scope="col">Code</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">Food and Drink</th>
            <td>Milano</td>
            <td>MI</td>
            <td>Via Milano 22</td>
            <td>11111</td>
            <td>asdfg678asdfghjhg</td>
          </tr>
        </tbody>
      </table>

      <h2 className="mt-5">My Details</h2>
      <table className="table table-hover">
        <thead>
          <tr>
            <th scope="col">Full Name</th>
            <th scope="col">Phone</th>
            <th scope="col">Fiscal Code</th>
            <th scope="col">E-Mail</th>
            <th scope="col">Document</th>
            <th scope="col">Document ID</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">Paolo Bianchi</th>
            <td>3334445556</td>
            <td>PALRSS123ER456Y</td>
            <td>Paolobianchi22@gmail.com</td>
            <td>Passport</td>
            <td>asdfg6aa</td>
          </tr>
        </tbody>
      </table>

      <h2 className="mt-5">My Transactions</h2>
      <table className="table table-hover">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Date</th>
            <th scope="col">Time</th>
            <th scope="col">Causal</th>
            <th scope="col">Sender</th>
            <th scope="col">Import</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>01/02/2022</td>
            <td>10:03</td>
            <td>4</td>
            <td>Mario Rossi</td>
            <td>15 €</td>
          </tr>
          <tr>
            <th scope="row">1</th>
            <td>01/02/2022</td>
            <td>10:03</td>
            <td>4</td>
            <td>Mario Rossi</td>
            <td>15 €</td>
          </tr>
          <tr>
            <th scope="row">1</th>
            <td>01/02/2022</td>
            <td>10:03</td>
            <td>4</td>
            <td>Mario Rossi</td>
            <td>15 €</td>
          </tr>
          <tr>
            <th scope="row">1</th>
            <td>01/02/2022</td>
            <td>10:03</td>
            <td>4</td>
            <td>Mario Rossi</td>
            <td>15 €</td>
          </tr>
          <tr>
            <th scope="row">1</th>
            <td>01/02/2022</td>
            <td>10:03</td>
            <td>4</td>
            <td>Mario Rossi</td>
            <td>15 €</td>
          </tr>
          <tr>
            <th scope="row">1</th>
            <td>01/02/2022</td>
            <td>10:03</td>
            <td>4</td>
            <td>Mario Rossi</td>
            <td>15 €</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default Merchant;
