
export const nameRegex = new RegExp(
  /^[a-zA-Z]*$/,
);

export const phoneRegex = new RegExp(
  /^([+]39)?([\d]{10})$/,
);

export const emailRegex = new RegExp(
  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
);

export const fiscalRegex = new RegExp(
  /^([A-Z]){3}([A-Z]){3}[\dLMNP-V]{2}([A-EHLMPR-T]([04LQ][1-9MNP-V]|[1256LMRS][\dLMNP-V])|[DHPS][37PT][0L]|[ACELMRT][37PT][01LM])([A-ILMZ]{1}([0-9L-NP-V]){3})([A-Z]{1})$/, 'i',
);

export const pwdRegex = new RegExp(
  /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
);

/*
Must contain minimum 8 characters, at least one uppercase letter,
one lowercase letter, a number and a special character

  /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,

*/

export const passportRegex = new RegExp(
  /^[A-Za-z]{2}[0-9]{7}/,
);

export const idRegex = new RegExp(
  /^[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/,
);

export const cidRegex = new RegExp(
  /^[a-zA-Z]{2}[0-9]{5}[a-zA-Z]{2}$/,
);

export const licenseRegex = new RegExp(
  /^(([A-Z]{2}[0-9]{2})( )|([A-Z]{2}-[0-9]{2}))((19|20)[0-9][0-9])[0-9]{7}$/,
);

export const orRegex = new RegExp(
  /(^[A-Za-z]{2}[0-9]{7})|(^[a-zA-Z]{2}[0-9]{5}[a-zA-Z]{2})|(^(([A-Z]{2}[0-9]{2})( )|([A-Z]{2}-[0-9]{2}))((19|20)[0-9][0-9])[0-9]{7})|(^[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx])/,
);
