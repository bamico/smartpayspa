import React from 'react';
import PropTypes from 'prop-types';
import errorMessage from '../../utils/models/errorCodes.json';

const ErrorCodes = (props) => {
  const showFieldName = (code) => {
    switch (code) {
    case 10001:
    case 10002:
    case 10003:
    case 10004:
    case 10009:
    case 10012: {
      return true;
    }
    default: {
      return false;
    }
    }
  };

  return (
  // TODO: Change to a modal containing all errors that stays open
    <>
      <button
        className="btn btn-danger position-fixed top-0 end-0 mx-5 my-3"
        style={{ zIndex: 100 }}
        type="button"
        data-bs-toggle="offcanvas"
        data-bs-target="#offcanvasTop"
        aria-controls="offcanvasTop"
      >
        Show Errors

      </button>
      <div
        className="offcanvas offcanvas-top col-6 rounded mx-auto my-2 show"
        data-bs-scroll="true"
        tabIndex="-1"
        id="offcanvasTop"
        aria-labelledby="offcanvasTopLabel"
      >
        <div className="offcanvas-header text-center rounded-top border-bottom">

          <h5 className="m-auto" id="offcanvasTopLabel">
            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="#ff6c61" className="bi bi-exclamation-triangle-fill" viewBox="0 0 16 22">
              <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
            </svg>
            Some errors occurred
          </h5>
          <button type="button" className="btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close" />
        </div>
        <div className="offcanvas-body py-2">
          <ul className="list-unstyled my-0 py-0">
            {props.errors.map(error => (
              <li className="py-2">
                {errorMessage[0][error.code]}

                {error.code === 10005 && (
                  <span>
                    :
                    {' '}
                    <b>{props.email}</b>
                  </span>
                )}
                {error.fieldName && showFieldName(error.code) && (
                  <span>
                    :
                    {' '}
                    <b>{error.fieldName.replace('.', ' ')}</b>
                  </span>
                )}
              </li>
            ))}
          </ul>
        </div>
      </div>

    </>
  );
};

ErrorCodes.propTypes = {
  errors: PropTypes.array.isRequired,
  email: PropTypes.string,
};

ErrorCodes.defaultProps = {
  email: '',
};

export default ErrorCodes;
