/**
 *
 * Footer
 *
 * @version 1.0.0
 * @author b.amico
 * @since 02/08/2022
 */

import React from 'react';
// import formattedMessage from './languageModule';

const Footer = () => (
  <>
    <div className="d-flex flex-column footer-bground">
      <footer className="w-100 py-2 flex-shrink-0">
        <div className="container py-2">
          <div className="row gy-4 gx-5">
            <div className="col-lg-4 col-md-6">
              <h5 className="h1 text-white">SP.</h5>
              <p className="small text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
              <p className="small text-muted mb-0">
                &copy; Copyrights 2022. All rights reserved.
                {' '}
                {/* <a className="text-primary" href="/">Reply Atlas</a> */}
              </p>
            </div>
            <div className="col-lg-4 col-md-6">
              <h5 className="text-white mb-3">Quick links</h5>
              <ul className="list-unstyled text-muted">
                {/* <li><a href="/">Terms</a></li> */}
                <li><a href="https://www.nexi.it/privacy.html">Privacy</a></li>
                {/* <li><a href="/">FAQ</a></li> */}
              </ul>
            </div>
            <div className="col-lg-4 col-md-6">
              <h5 className="text-white mb-3">Newsletter</h5>
              <p className="small text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </>
);

export default Footer;
