import React from 'react';
import { FormattedMessage } from 'react-intl';

const formattedMessage = {
  home:
  <FormattedMessage
    id="smartpay.menu.home"
    defaultMessage="Home"
  />,
};

export default formattedMessage;
