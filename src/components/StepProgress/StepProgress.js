/* eslint-disable max-len */
import React from 'react';
import PropTypes from 'prop-types';


const StepProgress = (props) => {
  // variables to change colors dinamically
  const completed = ' completed';
  const white = ' white';

  // variables that contain styling for the stepper item
  let step1 = `stepper-item`;
  let step2 = 'stepper-item';
  let step3 = 'stepper-item';
  let step4 = 'stepper-item';
  let step5 = 'stepper-item';

  // variables that contain styling for the step counter
  let stepCount1 = 'step-counter';
  let stepCount2 = 'step-counter';
  let stepCount3 = 'step-counter';
  let stepCount4 = 'step-counter';
  let stepCount5 = 'step-counter';

  // variables that contain styling for the stepper bar
  let stepBar1 = 'stepper-bar';
  let stepBar2 = 'stepper-bar';
  let stepBar3 = 'stepper-bar';
  let stepBar4 = 'stepper-bar';

  // set item to blue and icon to white when the user is in step 1
  if (props.myprop > 0) {
    step1 += completed;
    stepCount1 += white;
  }

  // set bar and item to blue and icon to white when the user is in step 2
  if (props.myprop > 1) {
    step2 += completed;
    stepCount2 += white;
    stepBar1 += completed;
  }

  // set bar and item to blue and icon to white when the user is in step 3
  if (props.myprop > 2) {
    step3 += completed;
    stepCount3 += white;
    stepBar2 += completed;
  }

  // set bar and item to blue and icon to white when the user is in step 4
  if (props.myprop > 3) {
    step4 += completed;
    stepCount4 += white;
    stepBar3 += completed;
  }

  // set bar and item to blue and icon to white when the user is in step 4
  if (props.myprop > 4) {
    step5 += completed;
    stepCount5 += white;
    stepBar4 += completed;
  }

  return (
    <div className="stepper-wrapper">
      <div className={step1}>
        <div className={stepCount1}>
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-person-badge" viewBox="0 0 16 16">
            <path d="M6.5 2a.5.5 0 0 0 0 1h3a.5.5 0 0 0 0-1h-3zM11 8a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
            <path d="M4.5 0A2.5 2.5 0 0 0 2 2.5V14a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2.5A2.5 2.5 0 0 0 11.5 0h-7zM3 2.5A1.5 1.5 0 0 1 4.5 1h7A1.5 1.5 0 0 1 13 2.5v10.795a4.2 4.2 0 0 0-.776-.492C11.392 12.387 10.063 12 8 12s-3.392.387-4.224.803a4.2 4.2 0 0 0-.776.492V2.5z" />
          </svg>
        </div>
        <div className="step-name">Personal Data</div>
        <div className={stepBar1} />
      </div>

      <div className={step2}>
        <div className={stepCount2}>
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-credit-card-fill" viewBox="0 0 16 16">
            <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v1H0V4zm0 3v5a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V7H0zm3 2h1a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1v-1a1 1 0 0 1 1-1z" />
          </svg>
        </div>
        <div className="step-name">Document</div>
        <div className={stepBar2} />
      </div>

      <div className={step3}>
        <div className={stepCount3}>
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-shop-window" viewBox="0 0 16 16">
            <path d="M2.97 1.35A1 1 0 0 1 3.73 1h8.54a1 1 0 0 1 .76.35l2.609 3.044A1.5 1.5 0 0 1 16 5.37v.255a2.375 2.375 0 0 1-4.25 1.458A2.371 2.371 0 0 1 9.875 8 2.37 2.37 0 0 1 8 7.083 2.37 2.37 0 0 1 6.125 8a2.37 2.37 0 0 1-1.875-.917A2.375 2.375 0 0 1 0 5.625V5.37a1.5 1.5 0 0 1 .361-.976l2.61-3.045zm1.78 4.275a1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0 1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0 1.375 1.375 0 1 0 2.75 0V5.37a.5.5 0 0 0-.12-.325L12.27 2H3.73L1.12 5.045A.5.5 0 0 0 1 5.37v.255a1.375 1.375 0 0 0 2.75 0 .5.5 0 0 1 1 0zM1.5 8.5A.5.5 0 0 1 2 9v6h12V9a.5.5 0 0 1 1 0v6h.5a.5.5 0 0 1 0 1H.5a.5.5 0 0 1 0-1H1V9a.5.5 0 0 1 .5-.5zm2 .5a.5.5 0 0 1 .5.5V13h8V9.5a.5.5 0 0 1 1 0V13a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V9.5a.5.5 0 0 1 .5-.5z" />
          </svg>
        </div>
        <div className="step-name">Shop Info</div>
        <div className={stepBar3} />
      </div>

      <div className={step4}>
        <div className={stepCount4}>
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-asterisk" viewBox="0 0 16 16">
            <path d="M8 0a1 1 0 0 1 1 1v5.268l4.562-2.634a1 1 0 1 1 1 1.732L10 8l4.562 2.634a1 1 0 1 1-1 1.732L9 9.732V15a1 1 0 1 1-2 0V9.732l-4.562 2.634a1 1 0 1 1-1-1.732L6 8 1.438 5.366a1 1 0 0 1 1-1.732L7 6.268V1a1 1 0 0 1 1-1z" />
          </svg>
        </div>
        <div className="step-name">Password</div>
        <div className={stepBar4} />
      </div>

      <div className={step5}>
        <div className={stepCount5}>
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-check-lg" viewBox="0 0 16 16">
            <path d="M12.736 3.97a.733.733 0 0 1 1.047 0c.286.289.29.756.01 1.05L7.88 12.01a.733.733 0 0 1-1.065.02L3.217 8.384a.757.757 0 0 1 0-1.06.733.733 0 0 1 1.047 0l3.052 3.093 5.4-6.425a.247.247 0 0 1 .02-.022Z" />
          </svg>
        </div>
        <div className="step-name">Finish</div>
      </div>
    </div>
  );
};

StepProgress.propTypes = {
  // Add here some propTypes
  myprop: PropTypes.number,
};

StepProgress.defaultProps = {
  // Add here some default propTypes values
  myprop: 1,
};

export default StepProgress;
