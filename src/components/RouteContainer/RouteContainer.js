/**
 *
 * RouteContainer
 *
 */
import React from 'react';
import PropTypes from 'prop-types';

const RouteContainer = props => (
  <>
    <div className="container defaultContainer">
      <div className="row">
        {props.children}
      </div>
    </div>
  </>
);

RouteContainer.propTypes = {
  // Add here some propTypes
  children: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
  ]).isRequired,
};

RouteContainer.defaultProps = {
  // Add here some default
};

export default RouteContainer;
