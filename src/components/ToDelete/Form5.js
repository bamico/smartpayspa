import React, { useContext } from 'react';
import { useForm } from 'react-hook-form';
import { ErrorMessage } from '@hookform/error-message';
import FormDataContext from '../../store/form-context';
// import { idRegex, licenseRegex, passportRegex } from './Regex';


function Form5() {
  const formCtx = useContext(FormDataContext);

  const { register, formState, handleSubmit } = useForm({ mode: 'onBlur', criteriaMode: 'all' });
  const { isDirty, isValid, errors } = formState;

  const onSubmit = (data) => {
    console.log(data);
    formCtx.addForm5Data(data);
    formCtx.plusStep();
  };

  // function to go back 1 step into the form
  const goBack = () => {
    formCtx.minusStep();
  };

  return (

    <>


      <div className="container py-5 d-flex justify-content-center">
        <div className="row">
          <div className="col-12 d-flex flex-column h-lg-100 card-custom px-5 me-5">
            <form className="col-12" onSubmit={handleSubmit(onSubmit)}>
              <div className="container" style={{ width: '400px' }}>
                <div className="row justify-content-center">
                  <div className="col-12 py-1">
                    <div>
                      <p>Choose a security question. It will be used to recover your password is case of troubles.</p>
                    </div>
                    <label className="form-label py-1 px-2" htmlFor="question">Question:</label>
                    <select
                      className="form-select w-50 mx-auto"
                      style={{
                        border: errors.question ? '1px solid red' : '',
                      }}
                      {...register('question', {
                        required: 'This field is required',
                      })}
                    >
                      <option value="">Choose a question</option>
                      <option value="q1">What is the name of your childhood pet?</option>
                      <option value="q2">What high school did you attend?</option>
                      <option value="q3">What is the name of your first school?</option>
                      <option value="q4">What was your favourite food as a child?</option>
                    </select>
                    <ErrorMessage
                      errors={errors}
                      name="question"
                      render={({ messages }) => messages && Object.entries(messages).map(([type, message]) => (
                        <p key={type} className="error">{message}</p>
                      ))
                      }
                    />
                  </div>

                  <div className="col-12 py-1">
                    <div>
                      <label className="form-label py-1 px-2" htmlFor="answer">Your answer:</label>
                    </div>
                    <input
                      placeholder="Type here"
                      type="text"
                      style={{
                        border: errors.answer ? '1px solid red' : '',
                      }}
                      {...register('answer', {
                        required: true,
                        minLength: 2,
                      })}
                    />
                    <ErrorMessage
                      errors={errors}
                      name="answer"
                      render={({ messages }) => messages && Object.entries(messages).map(([type, message]) => (
                        <p key={type} className="error">{message}</p>
                      ))
                      }
                    />
                  </div>

                  <div className="fields pt-4 mt-1 mb-5 d-flex justify-content-around">
                    <button className="btn btn-primary" type="button" onClick={goBack}>{'<-Back '}</button>
                    <button
                      className="btn btn-primary"
                      type="submit"
                      disabled={!isDirty || !isValid}
                    >
                      Submit
                    </button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>

  );
}

export default Form5;
