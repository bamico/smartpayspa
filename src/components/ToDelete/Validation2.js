const Validation2 = (docType, docId, docExpire) => {
  // array to manage the various errors
  const errors = ['false', 'false', 'false', 'yes'];

  // wrong input validations

  // REQUIRED FIELDS

  // docType
  if (docType === 'placeholder') {
    errors[0] = 'empty';
    errors[3] = 'no';
  }

  // docId
  if (docId.trim().length === 0) {
    errors[1] = 'empty';
    errors[3] = 'no';
  }

  // docExpire
  if (docExpire.trim().length === 0) {
    errors[2] = 'empty';
    errors[3] = 'no';
  }

  return errors;
};

export default Validation2;
