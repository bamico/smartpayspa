const Validation = (name, surname, phone, id, email) => {
  // array to manage the various errors
  const errors = ['false', 'false', 'false', 'false', 'false', 'yes'];

  // wrong input validations

  // name
  if (!/^[a-zA-Z]*$/.test(name)) {
    errors[0] = 'true';
    errors[5] = 'no';
  } else {
    errors[0] = 'false';
  }

  // surname
  if (!/^[a-zA-Z]*$/.test(surname)) {
    errors[1] = 'true';
    errors[5] = 'no';
  } else {
    errors[1] = 'false';
  }

  // phone
  if (!/^([+]39)?([\d]{10})$/.test(phone)) {
    errors[2] = 'true';
    errors[5] = 'no';
  } else {
    errors[2] = 'false';
  }

  // id
  // eslint-disable-next-line max-len
  if (!/^([A-Z]){3}([A-Z]){3}[\dLMNP-V]{2}([A-EHLMPR-T]([04LQ][1-9MNP-V]|[1256LMRS][\dLMNP-V])|[DHPS][37PT][0L]|[ACELMRT][37PT][01LM])([A-ILMZ]{1}([0-9L-NP-V]){3})([A-Z]{1})$/i.test(id)) {
    errors[3] = 'true';
    errors[5] = 'no';
  } else {
    errors[3] = 'false';
  }

  // email
  // eslint-disable-next-line max-len
  if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)) {
    errors[4] = 'true';
    errors[5] = 'no';
  } else {
    errors[4] = 'false';
  }

  // REQUIRED FIELDS

  // name
  if (name.trim().length === 0) {
    errors[0] = 'empty';
    errors[5] = 'no';
  }

  // surname
  if (surname.trim().length === 0) {
    errors[1] = 'empty';
    errors[5] = 'no';
  }

  // phone
  if (phone.trim().length === 0) {
    errors[2] = 'empty';
    errors[5] = 'no';
  }

  // id
  if (id.trim().length === 0) {
    errors[3] = 'empty';
    errors[5] = 'no';
  }

  // email
  if (email.trim().length === 0) {
    errors[4] = 'empty';
    errors[5] = 'no';
  }

  return errors;
};

export default Validation;
