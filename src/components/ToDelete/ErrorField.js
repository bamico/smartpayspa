import React from 'react';
import PropTypes from 'prop-types';


function ErrorField(props) {
  // variable for the error
  let message = '';

  // change color for error display
  let color = '';

  // set message to empty
  if (props.data === 'empty') {
    message = 'This field is required';
    color = 'red';
  }

  // set message to error
  if (props.data === 'true') {
    message = 'This field has an error';
    color = 'red';
  }

  return (
    <div className={`${color} ${props.className}`}>
      <span id="error-first-name">
        {message}
      </span>
    </div>
  );
}

ErrorField.propTypes = {
  className: PropTypes.string,
  data: PropTypes.string,
};

ErrorField.defaultProps = {
  className: '',
  data: '',
};
export default ErrorField;
