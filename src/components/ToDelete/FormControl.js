import React, { useContext, useEffect, useState } from 'react';
import Form1 from './Form1';
import Form2 from './Form2';
import Form3 from './Form3';
import Form4 from './Form4';
import ConfirmModal from '../ConfirmModal';
import FormDataContext from '../../store/form-context';
import FormSent from './UI/FormSent';
import SendData from '../SendData';
import StepProgress from '../UI/StepProgress';

const FormControl = () => {
  const formCtx = useContext(FormDataContext);
  const [modal, setModal] = useState(false);
  formCtx.setUrl(window.location.href);
  const useMountEffect = fun => useEffect(fun, []);

  useMountEffect(() => { formCtx.stepReset(); });

  const [execute, setExecute] = useState(true);

  if (execute === true) {
    if (formCtx.step === 4) {
      const userData = formCtx.addData();
      SendData(userData);
      setExecute(false);
    }
  }

  const reset = () => {
    formCtx.stepReset();
  };

  const setModalTrueHandler = () => {
    setModal(true);
  };

  const setModalFalseHandler = () => {
    setModal(false);
  };

  return (
    <>
      <StepProgress />
      <div className="container-xl px-4">
        <div className="row justify-content-center">
          <div className="col-lg-7">
            <div className="card shadow-lg border-0 rounded-lg mt-5">
              <div className="card-header justify-content-center">
                <h3 className="fw-light my-4">{formCtx.formTitle}</h3>
              </div>
              <div className="card-body">
                {formCtx.step === 1 && <Form1 />}
                {formCtx.step === 2 && <Form2 />}
                {formCtx.step === 3 && <Form3 />}
                {formCtx.step === 4 && <Form4 />}
                {formCtx.step === 5 && <FormSent />}
              </div>
              <div className="card-footer text-center">
                <div className="small">
                  <div className="row justify-content-center">
                    {modal && (
                      <ConfirmModal
                        onDeny={setModalFalseHandler}
                        onConfirm={reset}
                        title="Are you sure?"
                        message="Do you want to stop the form compilation? This will delete all the progress you made so far."
                        buttonDeny="Cancel"
                        buttonConfirm="Confirm"
                      />
                    )}
                    <button
                      onClick={setModalTrueHandler}
                      className="btn btn-danger"
                      type="button"
                    >
                      Stop Compiling the form

                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default FormControl;
