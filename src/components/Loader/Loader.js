/**
 *
 * Banner
 *
 * @version 1.0.0
 * @author b.amico
 * @since 23/03/2022
 */

/**
 *
 * LoadingDots
 *
 */
import React from 'react';

const Loader = () => (
  <>
    <div className="container loadingPage">
      <div className="loadingContainer">
        <div className="loadingDot" />
        <div className="loadingDot" />
        <div className="loadingDot" />
      </div>
    </div>
  </>
);

export default Loader;
