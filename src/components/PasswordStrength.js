/* eslint-disable react/prop-types */
import React from 'react';


const PasswordStrength = (props) => {
  const password = props.pwd;
  console.log(password);

  const color = ['btn btn-danger rounded-circle mx-2 p-2', 'btn btn-danger rounded-circle mx-2 p-2', 'btn btn-danger rounded-circle mx-2 p-2', 'btn btn-danger rounded-circle mx-2 p-2', 'btn btn-danger rounded-circle mx-2 p-2'];

  const regex = new Array([]);
  regex.push('[A-Z]');
  regex.push('[a-z]');
  regex.push('[0-9]');
  regex.push('[$@$!%*#?&]');

  console.log(regex);

  const checkArray = [false, false, false, false, false, false];

  for (let i = 1; i < regex.length; i += 1) {
    console.log(i);
    if (new RegExp(regex[i]).test(password)) {
      checkArray[i] = true;
      color[i - 1] = 'btn btn-primary mx-2 p-2 rounded-circle';
    }
  }

  if (password.length > 7) {
    checkArray[5] = true;
    color[4] = 'btn btn-primary mx-3 p-2 rounded-circle';
  }

  const smallText = 'Click on the circles above to see how to create a valid and secure password';

  const AApressed = () => {
    document.getElementById('small-text').innerHTML = 'Use at least 1 upper case letter';
  };

  const aaPressed = () => {
    document.getElementById('small-text').innerHTML = 'Use at least 1 lower case letter';
  };

  const numberPressed = () => {
    document.getElementById('small-text').innerHTML = 'Use at least 1 number';
  };

  const specialPressed = () => {
    document.getElementById('small-text').innerHTML = 'Use at least 1 special character(!$%&)';
  };

  const lengthPressed = () => {
    document.getElementById('small-text').innerHTML = 'The password must be at least 8 characters long';
  };

  return (
    <div>
      <div className="my-3">
        <span onClick={AApressed} className={color[0]}>
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-up" viewBox="0 0 16 16">
            <path fillRule="evenodd" d="M8 15a.5.5 0 0 0 .5-.5V2.707l3.146 3.147a.5.5 0 0 0 .708-.708l-4-4a.5.5 0 0 0-.708 0l-4 4a.5.5 0 1 0 .708.708L7.5 2.707V14.5a.5.5 0 0 0 .5.5z" />
          </svg>
        </span>
        <span onClick={aaPressed} className={color[1]}>
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-down-short" viewBox="0 0 16 16">
            <path fillRule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v5.793l2.146-2.147a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 1 1 .708-.708L7.5 10.293V4.5A.5.5 0 0 1 8 4z" />
          </svg>
        </span>
        <span onClick={numberPressed} className={color[2]}>
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-123" viewBox="0 0 16 16">
            <path d="M2.873 11.297V4.142H1.699L0 5.379v1.137l1.64-1.18h.06v5.961h1.174Zm3.213-5.09v-.063c0-.618.44-1.169 1.196-1.169.676 0 1.174.44 1.174 1.106 0 .624-.42 1.101-.807 1.526L4.99 10.553v.744h4.78v-.99H6.643v-.069L8.41 8.252c.65-.724 1.237-1.332 1.237-2.27C9.646 4.849 8.723 4 7.308 4c-1.573 0-2.36 1.064-2.36 2.15v.057h1.138Zm6.559 1.883h.786c.823 0 1.374.481 1.379 1.179.01.707-.55 1.216-1.421 1.21-.77-.005-1.326-.419-1.379-.953h-1.095c.042 1.053.938 1.918 2.464 1.918 1.478 0 2.642-.839 2.62-2.144-.02-1.143-.922-1.651-1.551-1.714v-.063c.535-.09 1.347-.66 1.326-1.678-.026-1.053-.933-1.855-2.359-1.845-1.5.005-2.317.88-2.348 1.898h1.116c.032-.498.498-.944 1.206-.944.703 0 1.206.435 1.206 1.07.005.64-.504 1.106-1.2 1.106h-.75v.96Z" />
          </svg>
        </span>
        <span onClick={specialPressed} className={color[3]}>
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-braces" viewBox="0 0 16 16">
            <path d="M2.114 8.063V7.9c1.005-.102 1.497-.615 1.497-1.6V4.503c0-1.094.39-1.538 1.354-1.538h.273V2h-.376C3.25 2 2.49 2.759 2.49 4.352v1.524c0 1.094-.376 1.456-1.49 1.456v1.299c1.114 0 1.49.362 1.49 1.456v1.524c0 1.593.759 2.352 2.372 2.352h.376v-.964h-.273c-.964 0-1.354-.444-1.354-1.538V9.663c0-.984-.492-1.497-1.497-1.6zM13.886 7.9v.163c-1.005.103-1.497.616-1.497 1.6v1.798c0 1.094-.39 1.538-1.354 1.538h-.273v.964h.376c1.613 0 2.372-.759 2.372-2.352v-1.524c0-1.094.376-1.456 1.49-1.456V7.332c-1.114 0-1.49-.362-1.49-1.456V4.352C13.51 2.759 12.75 2 11.138 2h-.376v.964h.273c.964 0 1.354.444 1.354 1.538V6.3c0 .984.492 1.497 1.497 1.6z" />
          </svg>
        </span>
        <span onClick={lengthPressed} className={color[4]}>
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-left-right" viewBox="0 0 16 16">
            <path fillRule="evenodd" d="M1 11.5a.5.5 0 0 0 .5.5h11.793l-3.147 3.146a.5.5 0 0 0 .708.708l4-4a.5.5 0 0 0 0-.708l-4-4a.5.5 0 0 0-.708.708L13.293 11H1.5a.5.5 0 0 0-.5.5zm14-7a.5.5 0 0 1-.5.5H2.707l3.147 3.146a.5.5 0 1 1-.708.708l-4-4a.5.5 0 0 1 0-.708l4-4a.5.5 0 1 1 .708.708L2.707 4H14.5a.5.5 0 0 1 .5.5z" />
          </svg>
        </span>
      </div>
      <div>
        <p><small id="small-text">{smallText}</small></p>
      </div>
    </div>
  );
};

export default PasswordStrength;
