/**
 *
 * ScrollToTop
 *
 */
import PropTypes from 'prop-types';

import { Component } from 'react';

class ScrollToTop extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  componentDidUpdate() {
    window.scrollTo(0, 0);
  }

  render() {
    return this.props.children;
  }
}

ScrollToTop.propTypes = {
  children: PropTypes.node.isRequired,
};

ScrollToTop.defaultProps = {
};

export default ScrollToTop;
