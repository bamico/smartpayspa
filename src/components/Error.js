import React from 'react';
import { Link } from 'react-router-dom';


function Home() {
  return (
    <>
      <div className="container my-5 py-5 d-flex justify-content-center">
        <div className="row">
          <div className="col-12 d-flex flex-column h-lg-100 px-5 me-5">
            <h1 className="py-5">This page does not exist</h1>
            <Link className="pb-5" to="/">
              <button type="button" className="btn btn-primary btn-lg px-5 py-2">
                Go to home
              </button>
            </Link>
          </div>
        </div>
      </div>
    </>
  );
}

export default Home;
