/* eslint-disable react/prop-types */
/**
 *
 * InputTextField
 *
 */
import React from 'react';
import { Control, Errors } from 'react-redux-form';
import PropTypes from 'prop-types';

const renderTextField = ({
  onPaste,
  onCopy,
  onCut,
  onChange,
  onKeyPress,
  ...custom
} = {}) => (
  <input
    {...custom}
    onPaste={onPaste}
    onCopy={onCopy}
    onCut={onCut}
    onChange={onChange}
    onKeyPress={onKeyPress}
  />
);

const InputTextField = props => (
  <>
    <label className="form-label" htmlFor={props.name}>{props.label}</label>
    <Control.text
      model={props.model}
      name={props.name}
      maxLength={props.length}
      component={renderTextField}
      type={props.type}
      mapProps={props.mapProps}
      validators={props.validators}
      asyncValidators={props.asyncValidators}
      placeholder={props.placeholder}
      autoComplete={props.autocomplete}
      disabled={props.disabled}
      onPaste={props.onPaste}
      onCopy={props.onCopy}
      onCut={props.onCut}
      onChange={props.onChange}
      onKeyPress={props.onKeyPress}
    />
    <Errors
      show={{ touched: true, focus: false, pristine: false }}
      model={props.model}
      className="invalid-feedback"
      messages={props.messages}
    />
  </>
);


InputTextField.propTypes = {
  placeholder: PropTypes.string,
  validators: PropTypes.object,
  label: PropTypes.string.isRequired,
  type: PropTypes.string,
  model: PropTypes.string.isRequired,
  mapProps: PropTypes.object,
  messages: PropTypes.object,
  asyncValidators: PropTypes.object,
  autocomplete: PropTypes.string,
  disabled: PropTypes.bool,
  length: PropTypes.number,
  onPaste: PropTypes.func,
  onCopy: PropTypes.func,
  onCut: PropTypes.func,
  onChange: PropTypes.func,
};

InputTextField.defaultProps = {
  type: 'text',
  mapProps: {},
  validators: {},
  asyncValidators: {},
  messages: {},
  placeholder: '',
  autocomplete: '',
  disabled: false,
  length: null,
  onPaste: () => true,
  onCopy: () => true,
  onCut: () => true,
  onChange: () => true,
};

export default InputTextField;
