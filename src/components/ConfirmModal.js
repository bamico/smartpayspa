import React from 'react';
import PropTypes from 'prop-types';

const ConfirmModal = props => (
  <>
    <div className="backdrop d-flex justify-content-center align-items-center" onClick={props.onDeny}>
      <div className="card py-3 w-25" style={{ width: '18rem' }}>
        <header className="card-title">
          <h2 className="px-3">{props.title}</h2>
        </header>
        <div className="card-body card-footer">
          <p>{props.message}</p>
        </div>
        <footer className="card-body d-flex flex-row-reverse">
          <button className="btn btn-outline-danger mx-2" type="button" onClick={props.onConfirm}>{props.buttonConfirm}</button>
          {props.buttonDeny !== 'none' && <button className="btn btn-outline-primary" type="button" onClick={props.onDeny}>{props.buttonDeny}</button>}
        </footer>
      </div>
    </div>
  </>
);

ConfirmModal.propTypes = {
  onConfirm: PropTypes.func,
  onDeny: PropTypes.func,
  title: PropTypes.string,
  message: PropTypes.string,
  buttonDeny: PropTypes.string,
  buttonConfirm: PropTypes.string,
};

ConfirmModal.defaultProps = {
  onConfirm: undefined,
  onDeny: undefined,
  title: '',
  message: '',
  buttonDeny: '',
  buttonConfirm: '',
};

export default ConfirmModal;
