import axios from 'axios';
import React from 'react';


export default class Users extends React.Component {
  constructor() {
    super();
    this.state = {
      allUsers: [],
      filteredUsers: [],
      currentPage: 1,
      currentPageUsers: [],
    };
    this.searchUsers = this.searchUsers.bind(this);
    this.paginationNext = this.paginationNext.bind(this);
    this.paginationPrev = this.paginationPrev.bind(this);
    this.firstPage = this.firstPage.bind(this);
    this.lastPage = this.lastPage.bind(this);
  }


  componentDidMount() {
    // Function to censor sensible data
    function censorData(user) {
      function doesPrefixExist(string) {
        if (string.charAt(0) === '+') {
          return 7;
        }
        return 3;
      }

      function asteriskReplacer(string, charsToKeep) {
        let censoredString = string.substr(0, charsToKeep);

        for (let i = 0; i < string.length - (charsToKeep + 1); i += 1) {
          censoredString += '*';
        }
        censoredString += string.charAt(string.length - 1);
        return censoredString;
      }

      user.id = asteriskReplacer(user.id, 1);
      user.phone = asteriskReplacer(user.phone, doesPrefixExist(user.phone));

      return user;
    }

    // HTTP request
    axios.get('https://test-7b314-default-rtdb.firebaseio.com/users.json')
      .then((response) => {
        // Converts the API data from object of objects to array of objects
        const allUsersObject = response.data;
        if (allUsersObject === null) {
          console.log('There are no users in the API.');
        } else {
          const userKeys = Object.keys(allUsersObject);
          const allUsers = [];
          userKeys.forEach((key, index) => {
            allUsersObject[key].key = key;
            allUsersObject[key].userid = index;
            censorData(allUsersObject[key]);
            allUsers.push(allUsersObject[key]);
          });
          this.setState({ allUsers });
          this.searchUsers('');
        }
      });
  }

  searchUsers(keyWord) {
    keyWord = keyWord.toLowerCase();
    const allUsers = this.state.allUsers.slice();
    const filteredUsers = [];
    allUsers.forEach((user) => {
      let toPush = false;
      Object.keys(user)
        .forEach((key) => {
          if (typeof (user[key]) === 'string') {
            if (user[key].toLowerCase().includes(keyWord) || keyWord === null) {
              toPush = true;
            }
          }
        });
      if (toPush) {
        filteredUsers.push(user);
      }
    });
    this.setState({ filteredUsers });
    this.setState({ currentPage: 1 }, this.currentPageUser());
    this.currentPageUser();
  }

  // Pagination Button
  paginationPrev() {
    console.log(`The current page was ${this.state.currentPage}`);
    if (this.state.currentPage > 1) {
      this.setState(prevState => ({ currentPage: prevState.currentPage - 1 }), this.currentPageUser());
    }

    console.log(`But now it's ${this.state.currentPage}`);
  }

  paginationNext() {
    console.log(`The current page was ${this.state.currentPage}`);
    if (this.state.currentPage < Math.ceil(this.state.filteredUsers.length / 10)) {
      this.setState(prevState => ({ currentPage: prevState.currentPage + 1 }), this.currentPageUser());
    } else console.log('No more users to show');
    console.log(`But now it's ${this.state.currentPage}`);
  }

  currentPageUser() {
    const currentPageUsers = [];
    this.state.filteredUsers.slice((this.state.currentPage - 1) * 10, this.state.currentPage * 10)
      .forEach((user) => {
        currentPageUsers.push(user);
      });
    this.setState({ currentPageUsers });
  }

  firstPage() {
    this.setState({ currentPage: 1 }, this.currentPageUser());
  }

  lastPage() {
    const filteredUsers = this.state.filteredUsers.slice();
    const lastPage = Math.ceil(filteredUsers.length / 10);
    this.setState({ currentPage: lastPage }, this.currentPageUser());
  }

  render() {
    return (
      <>

        <div className="container pt-5">
          <div className="row col-12">
            <div className="col-12 card-custom table-responsive">
              <div className="col-12 d-flex py-3 justify-content-end">
                <input
                  type="search"
                  className="search-input"
                  value={this.state.keyWord}
                  onChange={e => (this.searchUsers(e.target.value))}
                  placeholder="Enter keyword"
                />
              </div>
              <table className="table align-middle table-borderless table-striped table-hover">
                <thead className="">
                  <tr className="text-main">
                    <th>User ID</th>
                    <th>User Key</th>
                    <th>Name</th>
                    <th>Surname</th>
                    <th>Phone</th>
                    <th>Fiscal Code</th>
                    <th>Email</th>
                    <th>Details</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.currentPageUsers.map((user, index) => (
                    <>

                      <tr>
                        <td>
                          #
                          {user.userid}
                        </td>
                        <td>

                          {user.key}
                        </td>
                        <td>{user.name}</td>
                        <td>{user.surname}</td>
                        <td>{user.phone}</td>
                        <td>{user.id}</td>
                        <td>{Object.prototype.hasOwnProperty.call(user, 'email') ? user.email : 'N/A'}</td>
                        <td>
                          <button
                            type="button"
                            className="btn btn-primary"
                            data-bs-toggle="collapse"
                            data-bs-target={`#user-${index}`}
                            aria-expanded="false"
                            aria-controls={`user-${ index}`}
                            id={`user-button-${ index}`}
                            // onClick={console.log(`Button #${index} pressed`)}
                          >
                            Show details
                          </button>
                        </td>

                      </tr>
                      <tr>
                        <td colSpan="6">
                          <div className="collapse" id={`user-${index}`}>
                            <ul className="document-data d-flex justify-content-around align-items-center">
                              <li>{user.docType}</li>
                              <li>{user.docId}</li>
                              <li>
                                {`Expires on ${user.docExpire}`}
                              </li>
                            </ul>
                          </div>
                        </td>
                      </tr>
                    </>
                  ))}
                </tbody>
              </table>


              {/* Button for Pagination */}
              <div className="container pt-3">
                <div className="row">
                  <div className="col-12 input-group justify-content-center">
                    <ul className="paginator pagination pagination-lg justify-content-center">

                      {this.state.currentPage !== 1
                        ? (
                          <>
                            <li className="page-item">
                              <button type="button" className="page-link" onClick={this.firstPage}>First</button>
                            </li>
                            <li className="page-item">
                              <button
                                type="button"
                                className="page-link"
                                onClick={this.paginationPrev}
                              >
                                Previous

                              </button>
                            </li>
                          </>
                        )
                        : (
                          <>
                            <li className="page-item disabled">
                              <button type="button" className="page-link" onClick={this.firstPage}>First</button>
                            </li>
                            <li className="page-item disabled">
                              <button
                                type="button"
                                className="page-link"
                                onClick={this.paginationPrev}
                              >
                                Previous

                              </button>
                            </li>
                          </>
                        )
                      }

                      <li className="page-item">
                        {this.state.currentPage === Math.ceil(this.state.filteredUsers.length / 10) ? (
                          <button
                            type="button"
                            className="page-link"
                            onClick={this.paginationNext}
                          >
                            <p className="p-pagination-custom">{this.state.currentPage - 2}</p>

                          </button>
                        ) : null}
                      </li>
                      <li className="page-item">
                        {this.state.currentPage !== 1 ? (
                          <button
                            type="button"
                            className="page-link"
                            onClick={this.paginationPrev}
                          >
                            <p className="p-pagination-custom">{this.state.currentPage - 1}</p>

                          </button>
                        ) : null}

                      </li>
                      <li className="page-item active">
                        <button type="button" className="page-link">
                          <p className="p-pagination-custom">{this.state.currentPage}</p>
                        </button>
                      </li>
                      <li className="page-item">
                        {this.state.currentPage !== Math.ceil(this.state.filteredUsers.length / 10) ? (
                          <button
                            type="button"
                            className="page-link"
                            onClick={this.paginationNext}
                          >
                            <p className="p-pagination-custom">{this.state.currentPage + 1}</p>

                          </button>
                        ) : null}
                      </li>
                      <li className="page-item">
                        {this.state.currentPage === 1 ? (
                          <button
                            type="button"
                            className="page-link"
                            onClick={this.paginationPrev}
                          >
                            <p className="p-pagination-custom">{this.state.currentPage + 2}</p>

                          </button>
                        ) : null}

                      </li>
                      {this.state.currentPage !== Math.ceil(this.state.filteredUsers.length / 10)
                        ? (
                          <>
                            <li className="page-item">
                              <button type="button" className="page-link" onClick={this.paginationNext}> Next</button>
                            </li>
                            <li className="page-item">
                              <button type="button" className="page-link" onClick={this.lastPage}>
                                Last
                              </button>
                            </li>
                          </>
                        )
                        : (
                          <>
                            <li className="page-item disabled">
                              <button type="button" className="page-link" onClick={this.paginationNext}> Next</button>
                            </li>
                            <li className="page-item disabled">
                              <button type="button" className="page-link" onClick={this.lastPage}>
                                Last
                              </button>
                            </li>
                          </>
                        )
                      }

                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
