import axios from 'axios';
import FormData from 'form-data';
// import fs from 'fs';
import backEndURL from '../index.js';

// const SendData = (userData, docBackPath, docFrontPath, picPath) => {
const SendData = (userData) => {
  const formData = new FormData();
  formData.append('registration', userData);
  // formData.append('documentBack', fs.createReadStream(docBackPath));
  // formData.append('documentFront', fs.createReadStream(docFrontPath));
  // formData.append('picture', fs.createReadStream(picPath));
  // formData.append('documentBack', userData.docBack);
  // formData.append('documentFront', userData.docFront);
  // formData.append('picture', userData.userPic);


  // axios('https://test-7b314-default-rtdb.firebaseio.com/users.json',
  //   {
  //     method: 'post',
  //     data: userData,
  //   }).then((r) => {
  //   console.log('confirm send', r);
  // });

  axios.post(`${backEndURL}/register`, formData, {
    headers: formData.getHeaders(),
  }).then((res) => {
    if (res.status === 201) console.log(`Registered succesfully`, res);
    else {
      if (res.status === 400) console.log('Bad Request', res);
      if (res.status === 403) console.log('Unauthorized', res);
      if (res.status === 500) console.log('Server Error', res);
      else console.log(`Error ${res.status}`, res);
    }
  }).catch((error) => {
    console.log(`Error: ${error}`);
  });
};

export default SendData;
