import React from 'react';
import PropTypes from 'prop-types';

function ButtonAnchor(props) {
  return (
    <button
      className={`btn ${props.class}`}
      type={props.type === 'submit' ? 'submit' : 'button'}
      // eslint-disable-next-line consistent-return
      onClick={() => {
        if (props.onclick === undefined) return false;
        props.onclick();
      }}
      disabled={props.isDisabled ? 'disabled' : undefined}
    >
      {props.children}
    </button>
  );
}

ButtonAnchor.propTypes = {
  type: PropTypes.string,
  onclick: PropTypes.func,
  children: PropTypes.node.isRequired,
  class: PropTypes.string,
  isDisabled: PropTypes.bool,
};

ButtonAnchor.defaultProps = {
  onclick: undefined,
  class: '',
  type: 'button',
  isDisabled: false,
};

export default ButtonAnchor;
