/**
 *
 * Banner
 *
 * @version 1.0.0
 * @author b.amico
 * @since 02/08/2022
 */

import React from 'react';
import { Link } from 'react-router-dom';
import Button from '../UI/Button';

// import formattedMessage from './languageModule';

const Banner = () => (
  <>
    <div className="">
      <div className="container-fluid px-5 banner">
        <div className="row gx-5 align-items-center">
          <div className="col-lg-6">
            <div className="mb-5 mb-lg-0 text-center text-lg-start">
              <h1 className="display-1 lh-1 mb-3">Welcome to SmartPay</h1>
              <p className="lead fw-normal text-muted mb-5">
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Quod, odio recusandae. Reiciendis omnis unde maiores vitae, possimus aliquam corporis dignissimos?
                A repudiandae eaque alias voluptatum voluptates animi sint obcaecati doloremque.
              </p>
            </div>
          </div>
          <div className="col-lg-6">
            <div className="login-form">
              <div className="main-div">
                <div className="panel">
                  <p className="login-title">Area Personale</p>
                  <p className="login-subtitle">L`area personale è riservata ai possessori di utenza merchant</p>
                </div>
                <form id="Login">
                  <div className="form-group">
                    <input type="email" className="form-control" id="inputEmail" placeholder="Email Address" />
                  </div>
                  <div className="form-group">
                    <input type="password" className="form-control" id="inputPassword" placeholder="Password" />
                    <div className="lin-recovery" id="recoveryintro">
                      <a href="/" className="recover-lnk">Hai dimenticato le credenziali?</a>
                    </div>
                  </div>
                  <div className="action">
                    <div className="remember-box">
                      <input type="checkbox" id="user-remember" value="remember" className="grommetux-check-box__control" />
                      <label htmlFor="user-remember">Ricorda l’email al prossimo accesso</label>
                    </div>
                    <Button type="submit" className="btn-primary btn-lg">Accedi</Button>
                  </div>
                </form>
                <div className="login-banner__text">
                  <p>Sei il titolare di un negozio e vuoi gestire il tuo pos online?</p>
                </div>
                <div className="row">
                  <div className="col">
                    <Link to="form">
                      <span className="link-cta">REGISTRATI</span>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </>
);

export default Banner;
