/**
 *
 * RequirementBar
 *
 */
import React from 'react';
import PropTypes from 'prop-types';


export const RequirementsBar = props => (
  <>
    <div className="row">
      <div className="requirement">
        {props.requirementsList.map(element => (
          <div key={element.id} className={`requirementControl ${element.isChecked ? 'requirementControlChecked' : ''}`}>
            {element.label}
          </div>
        ))}
      </div>
    </div>
  </>
);

RequirementsBar.propTypes = {
  requirementsList: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      label: PropTypes.node,
      isChecked: PropTypes.bool,
    }),
  ),
};

RequirementsBar.defaultProps = {
  requirementsList: [],
};


export default RequirementsBar;
