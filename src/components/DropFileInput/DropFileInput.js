import React, { useState } from 'react';
import PropTypes from 'prop-types';

const DropFileInput = (props) => {
  const [fileList, setFileList] = useState([]);
  const [filesize, setFilesize] = useState([]);
  const [img, setImg] = useState([]);
  const [hasError, setError] = useState(false);
  const onFileDrop = (e) => {
    // eslint-disable-next-line no-debugger
    const newFile = e.target.files[0];
    const acceptedImageTypes = ['image/jpeg', 'image/png'];
    const newFilesize = parseFloat(newFile.size / (1024 * 1024)).toFixed(2);
    const newFiletype = newFile && acceptedImageTypes.includes(newFile.type);
    const reader = new FileReader(newFile);
    reader.readAsDataURL(newFile);
    const updatedList = [...fileList, newFile];
    if (newFile && newFilesize <= 2 && newFiletype) {
      setFilesize(newFilesize);
      // eslint-disable-next-line func-names
      reader.onloadend = function () {
        setImg(reader.result);
      };
      const error = document.getElementById(`error_${props.id}`);
      if (error) {
        error.remove();
      }
      setFileList(updatedList);
      props.onFileChange(updatedList);
    } else if (newFile && newFiletype === false) {
      setError(true);
      const para = document.createElement('p');
      para.className = 'fs-6 text-danger';
      const error = newFiletype === false ? document.createTextNode('Warning the file must be a png or a jpeg') : document.createTextNode('The file must be smaller than 2MB');
      para.appendChild(error);
      para.setAttribute('id', `error_P${props.id}`);
      setTimeout(() => {
        document.getElementById(`error_${props.id}`).appendChild(para);
        setFileList([]);
      }, 500);
      setTimeout(() => {
        para.remove();
        setError(false);
      }, 3500);
    } else if (newFile && newFilesize > 2) {
      setError(true);
      const para = document.createElement('p');
      para.className = 'fs-6 text-danger';
      const error = newFiletype === false ? document.createTextNode('The file must be smaller than 2MB') : document.createTextNode('The file must be smaller than 2MB');
      para.appendChild(error);
      para.setAttribute('id', `error_P${props.id}`);
      setTimeout(() => {
        document.getElementById(`error_${props.id}`).appendChild(para);
        setFileList([]);
      }, 100);
      setTimeout(() => {
        para.remove();
        setError(false);
      }, 3500);
    }
  };

  const fileRemove = (file) => {
    const updatedList = [...fileList];
    const fileToRemove = fileList.find(element => element === file);
    updatedList.splice(fileToRemove);
    setFileList(updatedList);
    props.onFileChange(updatedList);
  };
  return (
    <>
      {fileList.length === 0 ? (
        <div>
          <div className="drop-file-input">
            <div className="drop-file-input__label">
              <p>Drag & Drop your files here</p>
            </div>
            <input id="inputDrag" className="input-drop" type="file" value="" accept="image/*" onChange={onFileDrop} />
          </div>
          <div className="drop-file-preview" id={`error_${props.id}`} />
        </div>
      ) : ((fileList.map(item => (
        <div key={item}>
          <div className="d-flex justify-content-center">
            <div className="drop-file-input">
              <div className="drop-file-input__label">
                <div id={`gallery_${props.id}`} className="galery">
                  <img src={img} alt="" className="img-fluid img-preview" />
                </div>
              </div>
            </div>
          </div>
          {hasError ? ''
            : (
              <>
                <div className="drop-file-preview">
                  <div className="drop-file-preview__item">
                    <div className="text-drop">
                      <p className="dot-success text-light">&#10004;</p>
                      <p className="px-4">{item.name}</p>
                      <p className="opacity-75">
                        {filesize}
                        {' '}
                        MB
                      </p>
                    </div>
                    <div className="ps-5">
                      <span className="drop-file-preview__item__del" onClick={() => fileRemove(item)}>x</span>
                    </div>
                  </div>
                </div>
              </>
            )
          }
        </div>
      ))))}
    </>
  );
};

DropFileInput.propTypes = {
  onFileChange: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
};

export default DropFileInput;
