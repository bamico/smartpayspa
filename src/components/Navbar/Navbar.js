/**
 *
 * Navbar
 *
 * @version 1.0.0
 * @author
 * @since
 */

import React from 'react';
import { Link } from 'react-router-dom';
import formattedMessage from './languageModule';

const Navbar = () => (
  <>
    <nav id="mainNav" className="navbar navbar-expand-lg navbar-light fixed-top shadow-sm">
      <div className="container px-5">
        <a className="navbar-brand fw-bold" href="#page-top">SmartPay</a>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarResponsive"
          aria-controls="navbarResponsive"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          Menu
          <i className="bi-list" />
        </button>
        <div className="collapse navbar-collapse" id="navbarResponsive">
          <ul className="navbar-nav ms-auto me-4 my-3 my-lg-0">
            <li className="nav-item">
              <Link className="nav-link me-lg-3" to="/">{formattedMessage.home}</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link me-lg-3" to="/users">Users</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link me-lg-3" to="/">About</Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </>
);
export default Navbar;
