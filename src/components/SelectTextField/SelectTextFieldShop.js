/* eslint-disable react/prop-types */
/**
 *
 * SelectTextField
 *
 */
import React from 'react';
import { Control, Errors } from 'react-redux-form';
import PropTypes from 'prop-types';

const renderTextField = ({
  options,
  listName,
  ...custom
} = {}) => (
  <>
    <select {...custom} id={`data-${listName}`}>
      {options.map(d => (<option value={d.nome} key={`${d.codice}-${d.nome}`}>{d.nome}</option>))}
    </select>
  </>
);

const SelectTextField = props => (
  <>
    <label className="form-label" htmlFor={props.name}>{props.label}</label>
    <Control.text
      model={props.model}
      name={props.name}
      length={props.length}
      component={renderTextField}
      type={props.type}
      mapProps={props.mapProps}
      validators={props.validators}
      asyncValidators={props.asyncValidators}
      placeholder={props.placeholder}
      disabled={props.disabled}
      value={props.key}
      options={props.data}
      listName={props.name}
    />
    <Errors
      show={{ touched: true, focus: false, pristine: false }}
      model={props.model}
      className="invalid-feedback"
      messages={props.messages}
    />
  </>
);


SelectTextField.propTypes = {
  placeholder: PropTypes.string,
  validators: PropTypes.object,
  label: PropTypes.string.isRequired,
  type: PropTypes.string,
  model: PropTypes.string.isRequired,
  mapProps: PropTypes.object,
  messages: PropTypes.object,
  asyncValidators: PropTypes.object,
  disabled: PropTypes.bool,
  data: PropTypes.array.isRequired,
};

SelectTextField.defaultProps = {
  type: 'text',
  mapProps: {},
  validators: {},
  asyncValidators: {},
  messages: {},
  placeholder: '',
  disabled: false,
};

export default SelectTextField;
